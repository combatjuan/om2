from parameters import *


class Ping:
    id = 0
    name = "ping"

    def serialize(self):
        data = bytearray(1)

        data[0] = self.id

        return data


class Pong:
    id = 1
    name = "pong"

    def __init__(self, data):
        pass

    def __str__(self):
        return "Pong!"


class Set:
    id = 2
    name = "set"

    def __init__(self, parameter_name, value):
        self._param = parameter(parameter_name)
        self._value = int(value * 1000) if self._param.type == float else value

    def serialize(self):
        data = bytearray(6)

        data[0] = self.id
        data[1] = self._param[0]

        # All data is sent as 4 bytes little endian
        # Floating data is scaled by 1000
        data[2] = self._value & 0xFF
        data[3] = (self._value >> 8) & 0xFF
        data[4] = (self._value >> 16) & 0xFF
        data[5] = (self._value >> 24) & 0xFF

        return data


class Get:
    id = 3
    name = "get"

    def __init__(self, parameter_name):
        self._param = parameter(parameter_name)

    def serialize(self):
        data = bytearray(6)

        data[0] = self.id
        data[1] = self._param[0]

        return data


class Report:
    id = 4
    name = "report"

    def __init__(self, data):
        param = data[1]
        if param not in PARAMETERS:
            raise Exception(f"Received unknown parameter report: {param}")

        self._param = PARAMETERS[param]

        # All data is sent as 4 bytes little endian
        # Floating data is scaled by 1000
        self._value = data[2] | data[3] << 8 | data[4] << 16 | data[5] << 24
        if self._param.type is float:
            self._value = self._value * 1000

    @property
    def parameter(self):
        return self._param

    @property
    def value(self):
        return self._value

    def __str__(self) -> str:
        return f"Report: {self._param.name} = {self._value}"


class Stats:
    id = 5
    name = "stats"

    def __init__(self, data):
        param = data[1]
        if param not in PARAMETERS:
            raise Exception(f"Received unknown parameter report: {param}")

        self._param = PARAMETERS[param]

        count = data[2] | (data[3] << 8)

        self._values = []
        for i in range(count):
            b = 6 * i + 4

            # All data is sent as 2 byte timestamp + 4 bytes value -
            # all little endian. Floating data is scaled by 1000
            timestamp = data[b] | (data[b + 1] << 8)
            value = (
                data[b + 2]
                | (data[b + 3] << 8)
                | (data[b + 4] << 16)
                | (data[b + 5] << 24)
            )
            if self._param.type is float:
                value = value * 1000

            self._values.append((timestamp, value))

    @property
    def parameter(self):
        return self._param

    @property
    def values(self):
        return self._values

    def __str__(self) -> str:
        return f"Stats ({self._param.name}): {self._values}"


# All ids will be overwritten to match this
MESSAGES = [Ping, Pong, Set, Get, Report, Stats]

for i, m in zip(range(len(MESSAGES)), MESSAGES):
    m.id = i
    MESSAGES[i] = m


def message(message_name):
    return next(filter(lambda x: x.name == message_name, MESSAGES))
