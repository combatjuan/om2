class Parameter:
    def __init__(self, name, type):
        self._name = name
        self._type = type

    @property
    def type(self):
        return self._type

    @property
    def name(self):
        return self._name


# Note - this list needs to be the same order they're listed in the
# corresponding C++ enum
PARAMETERS = [
    Parameter("motor::status", int),
    Parameter("motor::rotation_direction", int),
    Parameter("motor::output_frequency_hz", float),
    Parameter("motor::actual_frequency_hz", float),
    Parameter("motor::output_current_a", float),
    Parameter("motor::torque_pct", int),
    Parameter("motor::power_kw", float),
    Parameter("motor::position", int),
    Parameter("motor::keypad_run_key_routing", int),
    Parameter("motor::frequency_source_setting", int),
    Parameter("motor::modbus_source_frequency_hz", float),
    Parameter("motor::run_command_source", int),
    Parameter("motor::base_frequency_hz", float),
    Parameter("motor::max_frequency_hz", float),
    Parameter("motor::jog_frequency_hz", float),
    Parameter("motor::jog_dc_brake_setting", int),
    Parameter("motor::dc_brake_setting", int),
    Parameter("motor::dc_brake_frequency_hz", float),
    Parameter("motor::dc_brake_wait_time_s", float),
    Parameter("motor::dc_brake_force_decel_pct", int),
    Parameter("motor::dc_brake_time_decel_s", float),
    Parameter("motor::accel_profile_switch_mode", int),
    Parameter("motor::accel_time_1_s", float),
    Parameter("motor::decel_time_1_s", float),
    Parameter("motor::accel_time_2_s", float),
    Parameter("motor::decel_time_2_s", float),
    Parameter("motor::accel_curve", int),
    Parameter("motor::decel_curve", int),
    Parameter("encoder::position", int),
]

PARAMETERS = {i: p for i, p in zip(range(len(PARAMETERS)), PARAMETERS)}


def parameter(param_name):
    return next(filter(lambda kv: kv[1].name == param_name, PARAMETERS.items()))
