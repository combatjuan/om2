#!/usr/bin/env python3

import argparse
import socket
import threading

from messages import *


def split_address(addr: str):
    ip, port = addr.split(":")
    return ip, int(port)


def parse_args():
    arg_parser = argparse.ArgumentParser(
        description="A tool for interacting with the OM2 P1AM software"
    )
    arg_parser.add_argument(
        "--remote",
        default="10.0.0.10:1234",
        type=str,
        help="Remote address and port to send UDP messages to",
    )
    arg_parser.add_argument(
        "--local",
        default="10.0.0.5:1234",
        type=str,
        help="Local address and port to receive UDP messages at",
    )

    return arg_parser.parse_known_args()


def main(args, unknown_args):
    print(f"Connecting to P1AM: {args.local} -> {args.remote}")

    shutdown = False

    local_ip, local_port = split_address(args.local)
    remote_ip, remote_port = split_address(args.remote)

    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp_socket.bind((local_ip, local_port))
    udp_socket.settimeout(0.5)

    def listen_udp():
        while not shutdown:
            try:
                data, addr = udp_socket.recvfrom(1024)
                # print(f"Got message: {data}")

                if data[0] >= len(MESSAGES):
                    print(f"Ignoring unknown message {data[0]}")
                    continue

                msg = MESSAGES[data[0]](data)
                print(msg)
                print("")
            except TimeoutError:
                pass

    listen_thread = threading.Thread(target=listen_udp)
    listen_thread.start()

    udp_socket.sendto(Ping().serialize(), (remote_ip, remote_port))

    while not shutdown:
        try:
            command = input("> ")
            command_parts = command.split(" ")

            msg_archetype = message(command_parts[0])
            msg = (
                msg_archetype(*command_parts[1:])
                if len(command_parts)
                else msg_archetype()
            )

            udp_socket.sendto(msg.serialize(), (remote_ip, remote_port))
        except KeyboardInterrupt:
            shutdown = True
        except EOFError:
            shutdown = True

    print("")
    listen_thread.join()


if __name__ == "__main__":
    main(*parse_args())
