#include "allocate.h"
#include "assert.h"

#include <FreeRTOS_SAMD21.h>

// Based on https://www.freertos.org/FreeRTOS_Support_Forum_Archive/February_2017/freertos_FreeRTOSs_compatibility_with_C_2aa2a047j.html
void * operator new( size_t size )
{
  auto* result = pvPortMalloc( size );
  ASSERT(result != nullptr);

  return result;
}

void * operator new[]( size_t size )
{
  auto* result = pvPortMalloc(size);
  ASSERT(result != nullptr);

  return result;
}

void operator delete( void * ptr )
{
  vPortFree ( ptr );
}

void operator delete[]( void * ptr )
{
  vPortFree ( ptr );
}