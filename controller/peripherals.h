#pragma once

// The base board defines min and max macros that don't jive
// with the standard library min/max functions
#undef min
#undef max

#include <cstdint>
#include <chrono>

namespace peripherals
{
enum class MotorState
{
  Forward,
  Reverse,
  JogForward,
  JogReverse,
  Stop
};

struct SystemState
{
  bool limit_switch = false;
  bool go_button = false;
  bool engage_button = false;
  bool configure_button = false;
  bool jog_left = false;
  bool jog_right = false;

  std::int32_t encoder_position = 0;
};

// Call this on startup before trying to do anything else
void init();

// Reads all the current system state information we need to figure out
// what events may have occurred
SystemState readSystemState();

void setLights(bool ready_state, bool problem_state, bool blink = false);
void setLightBlinkInterval(std::chrono::microseconds);
void setMotor(MotorState new_state);
void setLaunchSprinkler(bool on);
void setSlideSprinkler(bool on);
void resetEncoder();
}