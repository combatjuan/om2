#pragma once

#include <chrono>
#include <cmath>

#include "constants.h"
#include "ride_geometry.h"

using namespace std::chrono;

// Contains functions which seek to model the real world
namespace ride {

// All the functions spit out expected time in floating point seconds
using duration = std::chrono::duration<float>;

const float MPS_TO_MPH = 2.23694;
float mps_to_mph(float mps) {
  return mps * MPS_TO_MPH;
}

const float METERS_TO_FEET = 3.28084;
float m_to_ft(float m) {
  return m * METERS_TO_FEET;
}

ride::duration to_duration(float seconds)
{
  return duration_cast<ride::duration>(std::chrono::duration<float>(seconds));
}

ride::duration expected_travel_time(float run_speed_hz, float target_rotations)
{
  // Calculate timeout for travel period
  // Convert rotations to minutes using hz/to minutes calculation
  const auto reset_speed_rpm = run_speed_hz / config::MOTOR_HZ_PER_RPM * ride::DEFAULT_MOTOR_TO_PULLEY_RATIO;
  const auto travel_time = target_rotations / reset_speed_rpm * 60;

  return ride::duration(travel_time);
}

// TODO: We are going to hold this constant and make it not configurable
// But we should sanity check a value that doesn't cause the deceleration
// at the end of the ride to whip the handle around too much.
const float DECELERATION_GS = 1.0;

// This class calculates some values that we'll need to convert and send to the motor controller.
class Configuration {
public:
  // G-force in meters per second per second
  Configuration(float run_distance_m, float acceleration_gs, float deceleration_gs);

  float acceleration_gs() const { return _acceleration_gs; }
  ride::duration acceleration_time_s() const { return to_duration(_acceleration_time_s); }
  float acceleration_distance_m() const { return _acceleration_distance_m; }
  float max_speed_mps() const { return _max_speed_mps; }
  float deceleration_gs() const { return _deceleration_gs; }
  ride::duration deceleration_time_s() const { return to_duration(_deceleration_time_s); }
  float deceleration_distance_m() const { return _deceleration_distance_m; }

private:
  float _acceleration_gs;
  float _acceleration_time_s;
  float _acceleration_distance_m;
  float _max_speed_mps;
  float _deceleration_gs;
  float _deceleration_time_s;
  float _deceleration_distance_m;
};

// =============================================================================
// Configuration that may be changed per-ride
//
// We will target a maximum acceleration force as the variable.
// Using this and the geometry of the system we can calculate how
// quickly we need to accelerate, to what distance, and what 
Configuration::Configuration(float run_distance_m, float acceleration_gs, float deceleration_gs = DECELERATION_GS)
  :
  _acceleration_gs(acceleration_gs),
  _acceleration_time_s(0),
  _acceleration_distance_m(0),
  _max_speed_mps(0),
  _deceleration_gs(deceleration_gs),
  _deceleration_time_s(0),
  _deceleration_distance_m(0)
{
  //ASSERT(accel_gs > 0.0);
  //ASSERT(decel_gs > 0.0);

  float accel_mpss = acceleration_gs * 9.8;
  float decel_mpss = deceleration_gs * 9.8;

  // I did some algebra.  This is what I came up with.
  // First we calculate the max speed
  // WRONG!
  //_max_speed_mps = sqrt(2 * run_distance_m / (accel_mpss + decel_mpss));
  _max_speed_mps = sqrt((2 * run_distance_m * accel_mpss * decel_mpss) / (accel_mpss + decel_mpss));
  // Then with that known, everything else is pretty easy.
  _acceleration_time_s = _max_speed_mps / accel_mpss;
  _acceleration_distance_m = _max_speed_mps * _acceleration_time_s / 2.0;
  _deceleration_time_s = _max_speed_mps / decel_mpss;
  _deceleration_distance_m = _max_speed_mps * _deceleration_time_s / 2.0;
}


// This function calculates the amount of time that should be required to get to the point where
// the motor will stop.
//
// We will set the motor to a constant acceleration and assume that it mostly pulls it off
//
//     Max Speed ->     ____
//                |      /|
//                |     /..|   
//                |    /...|
//                |   /.....| 
//                |  /......|   <-- Shaded area is distance
//                | /........|  
//                |/.........|
//                +-------|--|-
//                      ^   ^
// Acceleration Time  -/    |
//                          /
// Deceleration Time  -----/
//

}
