#pragma once

namespace motor_control
{
//! \note These enums intentionally match the values reported by the device

enum class InverterStatus
{
  Error = -1,
  Initial = 0,
  Stopping = 2,
  Running = 3,
  FreeRunStop = 4,
  Jogging = 5,
  DcBraking = 6,
  Retrying = 7,
  Tripping = 8,
  Undervoltage = 9
};

enum class RotationDirection
{
  Error = -1,
  Stopped = 0,
  Forward = 1,
  Reverse = 2,
};

enum class FrequencySource
{
  Error = -1,
  KeypadPotentiometer = 0,
  CustomCircuitTerminalBlock = 1,
  DigitalOperator = 2,
  Modbus = 3,
  Option = 4,
  PulseTrain = 6,
  EasySequence = 7,
  OperationFunctionResult = 10
};

enum class RunKeyRouting
{
  Error = -1,
  Forward = 0,
  Reverse = 1
};

enum class RunCommandSource
{
  Error = -1,
  ControlCircuitTerminalBlock = 1,
  DigitalOperator = 2,
  Modbus = 3,
  Option = 4
};

enum class JogStopMode
{
  Error = -1,
  FreeRun_DisabledDuringOperation = 0,
  DecelerateAndStop_DisabledDuringOperation = 1,
  DcBrake_DisabledDuringOperation = 2,
  FreeRun_EnabledDuringOperation = 3,
  DecelerateAndStop_EnabledDuringOperation = 4,
  DcBrake_EnabledDuringOperation = 5,
};

enum class DcBrakeMode
{
  Error = -1,
  Disabled = 0,
  Enabled = 1,
  EnabledBelowTargetFrequency = 2
};

enum class AccelerationCurve
{
  Error = -1,
  Linear = 0,
  SCurve = 1,
  UCurve = 2,
  InvertedUCurve = 3,
  ELSCurve = 4
};

enum class AccelerationProfileSwitchMode
{
  Error = -1,
  TwoChannelInput = 0,
  AutoSwitchAtFrequency = 1,
  ForwardReverse = 2
};

enum class IntelligentInputSetting
{
  Error = -1,
  Forward = 0,
  Reverse = 1,
  Jog = 6,
};
}