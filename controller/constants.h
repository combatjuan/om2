#pragma once

#include "motor_control_enums.h"

// The base board defines min and max macros that don't jive
// with the standard library min/max functions
#undef min
#undef max

#include <chrono>
#include <cstdint>
#include <array>

namespace p1am
{
//! \note You have to include P1AM.h before constants.h for these
//! BUILTIN values to resolve. But for some reason, we can't just
//! include that header here directly
#ifdef LED_BUILTIN
constexpr static auto LED_PIN    = LED_BUILTIN;
#endif
#ifdef SWITCH_BUILTIN
constexpr static auto SWITCH_PIN = SWITCH_BUILTIN;
#endif

namespace eth
{
constexpr static std::uint16_t UDP_PORT_IN = 1234;
constexpr static std::uint16_t UDP_PORT_OUT = 1234;

constexpr static std::uint8_t CS_PIN = 5;

// MAC taken from the label on the side of the module
constexpr static std::array<std::uint8_t, 6> MAC_ADDR = {{0x60, 0x52, 0xD0, 0x07, 0x43, 0x1B}};
constexpr static std::array<std::uint8_t, 4> IP_ADDR = {{10, 0, 0, 10}};
}

namespace serial
{
namespace modbus
{
constexpr static std::uint8_t MOTOR_CONTROLLER_ADDRESS = 1;
}
}

namespace high_speed
{
constexpr static std::size_t FORWARD_CHANNEL = 1;
}

namespace digital
{
// I/O channels are 1-indexed
namespace in{
constexpr static std::size_t GO_BUTTON_CHANNEL = 1;
constexpr static std::size_t ENGAGE_BUTTON_CHANNEL = 5;
constexpr static std::size_t CONFIGURE_BUTTON_CHANNEL = 2;
constexpr static std::size_t JOG_LEFT_CHANNEL = 4;
constexpr static std::size_t JOG_RIGHT_CHANNEL = 3;
constexpr static std::size_t LIMIT_SWITCH_CHANNEL = 6;
}

namespace out
{
constexpr static std::size_t FORWARD_MOTOR_CHANNEL = 2;
constexpr static std::size_t REVERSE_MOTOR_CHANNEL = 3;

constexpr static std::size_t PROBLEM_LIGHT_CHANNEL = 1;
constexpr static std::size_t READY_LIGHT_CHANNEL = 4;

constexpr static std::size_t LAUNCH_SPRINKLER_CHANNEL = 5;
constexpr static std::size_t SLIDE_SPRINKLER_CHANNEL = 6;
}
}
}

// Startup delay
constexpr static auto TIMEOUT_INIT = std::chrono::seconds(1);

// Amount of time after stopping before shutting down sprinklers and whatnot
// and allowing return
constexpr static auto TIMEOUT_POST_FLIGHT = std::chrono::seconds(5);

// Multipled with the calculated time values to get timeouts
// that trigger the error state
constexpr static auto CALCULATED_TIMEOUT_RATIO = 1.2;

// What fraction of the distance traveled should be traveled
// quickly in reverse during reset
constexpr static float HIGH_SPEED_REVERSE_PERCENT = 0.75;
constexpr static float RESET_FAST_MOTOR_HZ = 10;
constexpr static float RESET_SLOW_MOTOR_HZ = 6;

// Set true to print loops/s every 5 seconds
constexpr static bool PRINT_AVERAGE_LOOP_RATE = false;
constexpr static std::size_t MAIN_LOOP_RATE_HZ = 200;

namespace messaging
{
enum class UdpHeader
{
  Ping,
  Pong,
  Set,
  Get,
  Report,
  Stats
};

enum class Parameter
{
  MotorStatus,
  MotorRotationDirection,
  MotorOutputFrequencyHz,
  MotorActualFrequencyHz,
  MotorOutputCurrentA,
  MotorTorquePct,
  MotorPowerKw,
  MotorPosition,
  MotorKeypadRunKeyRouting,
  MotorFrequencySourceSetting,
  MotorModbusSourceFrequencyHz,
  MotorRunCommandSource,
  MotorBaseFrequencyHz,
  MotorMaxFrequencyHz,
  MotorJogFrequencyHz,
  MotorJogDcBrakeSetting,
  MotorDcBrakeSetting,
  MotorDcBrakeFrequencyHz,
  MotorDcBrakeWaitTimeS,
  MotorDcBrakeForceDecelPct,
  MotorDcBrakeTimeDecelS,
  MotorAccelProfileSwitchMode,
  MotorAccelTime1S,
  MotorDecelTime1S,
  MotorAccelTime2S,
  MotorDecelTime2S,
  MotorAccelCurve,
  MotorDecelCurve,
  EncoderPosition,
};
}

// Controls the rate and amount of stats info collected by stats:: functions
namespace stats
{
// Sampling interval, and max amount of time that sampling should support
// without dumping data. This determines how much memory gets allocated
// for a block of sample data when we start sampling
//
// 20hz sampling - guess at how long returning will take
constexpr static auto SAMPLE_INTERVAL = std::chrono::milliseconds(50);
constexpr static auto MAX_SAMPLE_TIME = std::chrono::seconds(10);
constexpr static auto MAX_SAMPLE_COUNT = MAX_SAMPLE_TIME / SAMPLE_INTERVAL;
}

// Priority of threads, relative to idle priority
namespace priority
{
  constexpr static std::uint32_t MAIN_LOOP = 5;
  constexpr static std::uint32_t BLINK_LOOP = 1;
  constexpr static std::uint32_t STATS_LOOP = 2;
}

namespace config
{
  struct Options
  {
    // Set of speed options for the user - G-Force to target during acceleration
    float accel_gs;
  };

  // Ratio, of hz output from the controller
  // to rpms, as defined by the motor
  const static float MOTOR_HZ_PER_RPM = 60. / 1770.;

  constexpr static std::array<Options, 5> PRESETS = {
    {
      {0.01}, // 5mph, 21s
      {0.05}, // 10mph, 10s
      {0.1}, // 14mph, 7.0s
      {0.12}, // 15mph, 6.5s
      {0.17} // 17.5mph, 5.5s
    }
  };

namespace defaults
{
constexpr static auto OUTPUT_FREQUENCY_HZ = 0.0;
constexpr static auto ACCELERATION_TIME_1_S = 10.0;
constexpr static auto DECELERATION_TIME_1_S = 10.0;
constexpr static auto ACCELERATION_TIME_2_S = 10.0;
constexpr static auto DECELERATION_TIME_2_S = 1.0;
constexpr static auto FREQUENCY_SOURCE = motor_control::FrequencySource::Modbus;
constexpr static auto KEYPAD_RUN_ROUTING = motor_control::RunKeyRouting::Forward;
constexpr static auto RUN_COMMAND_SOURCE = motor_control::RunCommandSource::ControlCircuitTerminalBlock;
constexpr static auto BASE_FREQUENCY = 60.0;
constexpr static auto MAX_FREQUENCY = 100.0;
constexpr static auto JOG_FREQUENCY = 5.0;
constexpr static auto JOG_STOP_MODE = motor_control::JogStopMode::DcBrake_DisabledDuringOperation;
constexpr static auto DC_BRAKE_MODE = motor_control::DcBrakeMode::Enabled;
constexpr static auto DC_BRAKE_FREQUENCY_HZ = 0.0;
constexpr static auto DC_BRAKE_WAIT_TIME_S = 0.0;
constexpr static auto DC_BRAKE_FORCE_DECELERATION_PCT = 100;
constexpr static auto DC_BRAKE_TIME_DECELERATION_S = 1.0;
constexpr static auto ACCELERATION_PROFILE_SWITCH_MODE = motor_control::AccelerationProfileSwitchMode::ForwardReverse;
constexpr static auto ACCELERATION_CURVE = motor_control::AccelerationCurve::Linear;
constexpr static auto DECELERATION_CURVE = motor_control::AccelerationCurve::Linear;
constexpr static auto INTELLIGENT_INPUT_1 = motor_control::IntelligentInputSetting::Forward;
constexpr static auto INTELLIGENT_INPUT_2 = motor_control::IntelligentInputSetting::Reverse;
constexpr static auto INTELLIGENT_INPUT_3 = motor_control::IntelligentInputSetting::Jog;
}
}
