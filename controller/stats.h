#pragma once

#include "constants.h"
#include "motor_control.h"
#include "allocate.h"

// The base board defines min and max macros that don't jive
// with the standard library min/max functions
#undef min
#undef max

#include <chrono>
#include <array>
#include <memory>

namespace stats
{
using time_offset = std::chrono::duration<std::int16_t, std::milli>;

template<class T>
struct Sample
{
  time_offset timestamp;
  T value;
};

struct Samples
{
  std::size_t count = 0;
  std::array<Sample<std::int8_t>, MAX_SAMPLE_COUNT> motor_status;
  std::array<Sample<std::int16_t>, MAX_SAMPLE_COUNT> motor_torque;
  std::array<Sample<float>, MAX_SAMPLE_COUNT> motor_power;
  std::array<Sample<float>, MAX_SAMPLE_COUNT> motor_frequency_out;
  std::array<Sample<float>, MAX_SAMPLE_COUNT> motor_frequency_actual;
  std::array<Sample<std::int16_t>, MAX_SAMPLE_COUNT> motor_position;
  std::array<Sample<std::int16_t>, MAX_SAMPLE_COUNT> encoder_position;
};

// Don't use more than 3/4 of sram for stats
static_assert(sizeof(Samples) < 24576, "Your stats are too powerful!");

// Since we don't actually send commands to the motor controller at the same time as we're going
// to want to sample data, we can just pass off the motor controller object between the stats thread
// and the main loop thread. But the main loop thread is the thing reading the encoder, and we care
// about that here too. Rather than try to mutex that off or something, we're just going to have the
// main loop update a value here that represents the encoder value for the current direction and then
// the stats thread will save it at will
//
// This value is the sum of the forward and backward encoder values, such that if we drive forward
// a ways, and then back, it should return to 0
extern std::int32_t current_encoder_position;

// Start a stats thread that does nothing except between
// calls to start_collecting, stop_collecting
void init();

// Start the stats thread collecting data
//
// After starting collecting, you must stop collecting before
// starting it again
void start_collecting();

// Stop the stats thread collecting data
//
// Will no-op and return nullptr if start_collecting was not called
std::unique_ptr<Samples> stop_collecting();
}