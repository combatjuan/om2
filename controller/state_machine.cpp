#include "state_machine.h"

namespace state_machine
{
State update(State current_state, Event current_event)
{
  if(current_state == State::Init && current_event == Event::InitComplete)
  {
    return State::NotReady;
  }

  if(current_state == State::NotReady && current_event == Event::LimitSwitchRising)
  {
    return State::Ready;
  }

  if(current_state == State::NotReady && (current_event == Event::ConfigureButtonWithLimitSwitch || current_event == Event::ConfigureButtonWithoutLimitSwitch))
  {
    return State::Configuring;
  }
  
  if(current_state == State::Ready && current_event == Event::LimitSwitchFalling)
  {
    return State::NotReady;
  }

  if(current_state == State::Ready && (current_event == Event::ConfigureButtonWithLimitSwitch || current_event == Event::ConfigureButtonWithoutLimitSwitch))
  {
    return State::Configuring;
  }

  if(current_state == State::Configuring && current_event == Event::ConfigureButtonWithoutLimitSwitch)
  {
    return State::NotReady;
  }

  if(current_state == State::Configuring && current_event == Event::ConfigureButtonWithLimitSwitch)
  {
    return State::Ready;
  }

  if(current_state == State::Ready && current_event == Event::EngageButtonRising)
  {
    return State::Preflight;
  }

  if(current_state == State::Preflight && current_event == Event::EngageButtonFalling)
  {
    return State::Ready;
  }

  if(current_state == State::Preflight && current_event == Event::LimitSwitchFalling)
  {
    return State::NotReady;
  }

  if(current_state == State::Preflight && current_event == Event::GoButton)
  {
    return State::Forward;
  }

  if(current_state == State::Forward && current_event == Event::TargetDistanceReached)
  {
    return State::Stopping;
  }

  if(current_state == State::Stopping && current_event == Event::Timeout)
  {
    return State::Postflight;
  }

  if(current_state == State::Postflight && current_event == Event::Timeout)
  {
    return State::Complete;
  }

  if(current_state == State::Forward && current_event == Event::Timeout)
  {
    return State::TimedOut;
  }

  if(current_state == State::Complete && current_event == Event::GoButton)
  {
    return State::ResetFast;
  }

  if(current_state == State::ResetFast && current_event == Event::TargetDistanceReached)
  {
    return State::ResetSlow;
  }

  if(current_state == State::ResetFast && current_event == Event::Timeout)
  {
    return State::TimedOut;
  }

  if(current_state == State::ResetFast && current_event == Event::LimitSwitchRising)
  {
    return State::Ready;
  }

  if(current_state == State::ResetSlow && current_event == Event::LimitSwitchRising)
  {
    return State::Ready;
  }

  if(current_state == State::ResetSlow && current_event == Event::Timeout)
  {
    return State::TimedOut;
  }

  if(current_state == State::TimedOut && (current_event == Event::ConfigureButtonWithLimitSwitch || current_event == Event::ConfigureButtonWithoutLimitSwitch))
  {
    return State::NotReady;
  }

  return current_state;
}
}