#include "stats.h"

#include "assert.h"
#include "constants.h"
#include "recurring.h"
#include "thread.h"

#include <FreeRTOS_SAMD21.h>

#include <type_traits>
#include <utility>

using namespace std::chrono;

namespace stats
{
std::int32_t current_encoder_position = 0;
std::unique_ptr<Samples> samples;
rtos_clock::time_point sampling_start;

// Used to stop sampling and wait for sampling to end
// 1. Set waiting_task to your task handle, then set sampling_active = false, then use xTaskNotifyWait
bool sampling_active = false;
TaskHandle_t waiting_task;

TaskHandle_t tid_stats;
void stats_task(void*);

void init()
{
  xTaskCreate(&stats_task, "stats", 256, nullptr, tskIDLE_PRIORITY+priority::STATS_LOOP, &tid_stats);
}

void start_collecting()
{
  ASSERT(!samples);

  sampling_active = true;
  Serial.println("Starting stats collection...");
  xTaskNotify(tid_stats, 0, eNoAction);
  Serial.println("Stats started!");
}

std::unique_ptr<Samples> stop_collecting()
{
  if(samples)
  {
    waiting_task = xTaskGetCurrentTaskHandle();
    sampling_active = false;
    xTaskNotifyWait(0, 0, nullptr, portMAX_DELAY);
  }

  return std::move(samples);
}

template<class Predicate>
Sample<typename std::result_of<Predicate()>::type> collect_sample(Predicate&& p)
{
  const auto before = rtos_clock::now();
  const auto value = p();
  const auto after = rtos_clock::now();

  const auto value_time = before + (after - before)/2.0;
  const auto relative_time = duration_cast<milliseconds>(value_time - sampling_start);

  return {duration_cast<time_offset>(relative_time), value};
}

void collect_sample_set()
{
  auto& controller = MotorController::instance();

  ASSERT(samples);
  const auto index = samples->count;

  samples->motor_status[index]            = collect_sample([&]{return static_cast<std::int8_t>(controller.getInverterStatus());});
  samples->motor_torque[index]            = collect_sample([&]{return static_cast<std::int16_t>(controller.getTorquePercent());});
  samples->motor_power[index]             = collect_sample([&]{return controller.getPowerKw();});
  samples->motor_frequency_out[index]     = collect_sample([&]{return controller.getOutputFrequencyHz();});
  //samples->motor_frequency_actual[index]  = collect_sample([&]{return controller.getActualFrequencyHz();});
  //samples->motor_position[index]          = collect_sample([&]{return static_cast<std::int16_t>(controller.getPositionFeedback());});
  samples->encoder_position[index]        = collect_sample([&]{return static_cast<std::int16_t>(current_encoder_position);});
  samples->count++;
}

void stats_task(void*)
{
  while(true)
  {
    Serial.println("[stats] Awaiting command to sample...");
    xTaskNotifyWait(0, 0, nullptr, portMAX_DELAY);

    ASSERT(!samples);
    Serial.println("[stats] Allocating samples...");
    samples.reset(new Samples);

    sampling_start = rtos_clock::now();

    Serial.println("[stats] Begin sampling!");
    recurring(SAMPLE_INTERVAL,
      []{
        if(samples->count < samples->encoder_position.size())
        {
          collect_sample_set();
        }
        return sampling_active;
      }
    );

    Serial.println("[stats] End sampling!");
    xTaskNotify(waiting_task, 0, eNoAction);
  }
}
}