#pragma once

#include "motor_control_enums.h"

#include <cstdint>

namespace motor_control
{

// Motor behaves like a singleton - only one instance may ever
// be valid at a time. The first motor initialized will be valid;
// any others will not be. The motor can be moved to represent
// ownership between threads.
class MotorController
{
  public:
    static MotorController& instance();

    MotorController(const MotorController&) = delete;
    MotorController& operator=(const MotorController&) = delete;

    MotorController(MotorController&&) =  delete;
    MotorController& operator=(MotorController&&) =  delete;

    operator bool() const { return m_valid; };

    bool init();

    AccelerationProfileSwitchMode getAccelerationProfileSwitchMode();
    bool setAccelerationProfileSwitchMode(AccelerationProfileSwitchMode);

    IntelligentInputSetting getIntelligentInput1();
    bool setIntelligentInput1(IntelligentInputSetting value);

    IntelligentInputSetting getIntelligentInput2();
    bool setIntelligentInput2(IntelligentInputSetting value);

    IntelligentInputSetting getIntelligentInput3();
    bool setIntelligentInput3(IntelligentInputSetting value);

    float getAccelerationTime1Sec();
    bool setAccelerationTime1Sec(float);

    float getAccelerationTime2Sec();
    bool setAccelerationTime2Sec(float);

    float getDecelerationTime1Sec();
    bool setDecelerationTime1Sec(float);

    float getDecelerationTime2Sec();
    bool setDecelerationTime2Sec(float);

    AccelerationCurve getAccelerationCurve();
    bool setAccelerationCurve(AccelerationCurve);

    AccelerationCurve getDecelerationCurve();
    bool setDecelerationCurve(AccelerationCurve);

    InverterStatus getInverterStatus();

    FrequencySource getFrequencySource();
    bool setFrequencySource(FrequencySource);

    float getOutputFrequencyHz();
    bool setOutputFrequencyHz(float);

    float getActualFrequencyHz();

    float getBaseFrequencyHz();
    bool setBaseFrequencyHz(float);

    float getMaxFrequencyHz();
    bool setMaxFrequencyHz(float);

    float getJogFrequencyHz();
    bool setJogFrequencyHz(float);

    JogStopMode getJogStopMode();
    bool setJogStopMode(JogStopMode);

    float getOutputCurrentAmps();
    RotationDirection getRotationDirection();
    std::int32_t getTorquePercent();
    float getPowerKw();
    std::int32_t getPositionFeedback();

    RunKeyRouting getKeypadRunKeyRouting();
    bool setKeypadRunKeyRouting(RunKeyRouting);

    RunCommandSource getRunCommandSource();
    bool setRunCommandSource(RunCommandSource);

    DcBrakeMode getDcBrakeMode();
    bool setDcBrakeMode(DcBrakeMode);

    // Target frequency for braking mode of "below target frequency"
    float getDcBrakeFrequencyHz();
    bool setDcBrakeFrequencyHz(float);

    // 0-0.5 seconds
    float getDcBrakeWaitTimeSec();
    bool setDcBrakeWaitTimeSec(float);

    std::int32_t getDcBrakeDecelerationForcePercentage();
    bool setDcBrakeDecelerationForcePercentage(std::int32_t);

    // 0-60 seconds
    float getDcBrakeDecelerationTimeSec();
    bool setDcBrakeDecelerationTimeSec(float);

  private:
    MotorController();
    ~MotorController();

    static bool m_instance_exists;
    bool m_valid = false;
};
}

using motor_control::MotorController;