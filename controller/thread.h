#pragma once

#include <FreeRTOS_SAMD21.h>

// The base board defines min and max macros that don't jive
// with the standard library min/max functions
#undef min
#undef max

#include <chrono>

namespace std
{
namespace this_thread
{
template<class Rep, class Period>
void sleep_for(std::chrono::duration<Rep, Period> d)
{
  vTaskDelay( std::chrono::duration_cast<std::chrono::microseconds>(d).count() / portTICK_PERIOD_US );  
}
}
}

// Use recurring_sleep_for to perform a function on a loop with a regular timeout
// more accurately than using just "sleep_for" every time
template<class Rep, class Period>
void recurring_sleep_for(TickType_t& history, std::chrono::duration<Rep, Period> d)
{
  vTaskDelayUntil( &history, std::chrono::duration_cast<std::chrono::microseconds>(d).count() / portTICK_PERIOD_US );  
}

struct rtos_clock
{
  using duration = std::chrono::microseconds;
  using rep = duration::rep;
  using period = duration::period;
  using time_point = std::chrono::time_point<rtos_clock, duration>;

  constexpr static bool is_steady = true;

  static time_point now()
  {
    return time_point{duration{xTaskGetTickCount() * portTICK_PERIOD_US}};
  }
};

class rtos_mutex
{
  public:
    rtos_mutex() : m_handle(xSemaphoreCreateMutex()){}
    ~rtos_mutex()
    {
      vSemaphoreDelete(m_handle);
    }

    void lock()
    {
      xSemaphoreTake(m_handle, portMAX_DELAY);
    }

    void unlock()
    {
      xSemaphoreGive(m_handle);
    }

    rtos_mutex(const rtos_mutex&) = delete;
    rtos_mutex& operator=(const rtos_mutex&) = delete;
    rtos_mutex(rtos_mutex&&) = delete;
    rtos_mutex& operator=(rtos_mutex&&) = delete;

  private:
    SemaphoreHandle_t m_handle;
};

// Lock the mutex any time you're accessing a P1AM module, because they
// all go over the spi bus
extern rtos_mutex spi_bus_mutex;