#pragma once

#include <cstdint>

void * operator new( std::size_t size );

void * operator new[]( std::size_t size );

void operator delete( void * ptr );

void operator delete[]( void * ptr );