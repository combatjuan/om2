#include "constants.h"

#include "assert.h"
#include "motor_control.h"
#include "thread.h"

#include <ModbusRTUClient.h>
#include <P1AM_Serial.h>

// The base board defines min and max macros that don't jive
// with the standard library min/max functions
#undef min
#undef max

#include <utility>
#include <mutex>

namespace motor_control
{
ModbusRTUClientClass modbus_client;

struct Function
{
  std::uint32_t address;
  float scale;
};

constexpr static Function FUNCTIONS[] = {
  {0x0003, 1},    // INVERTER_STATUS
  {0x1004, 1},    // ROTATION_DIRECTION
  {0x0002, 0.01}, // OUTPUT_FREQUENCY
  {0x1003, 0.01}, // OUTPUT_CURRENT
  {0x100B, 0.01}, // ACTUAL_FREQUENCY
  {0x1010, 1},    // TORQUE
  {0x1012, 0.1},  // POWER
  {0x1038, 1},    // POSITION
  {0x1107, 1},    // KEYPAD_RUN_KEY_ROUTING
  {0x1201, 1},    // FREQUENCY_SOURCE_SETTING
  {0x1202, 1},    // RUN_COMMAND_SOURCE
  {0x1203, 0.1},  // BASE_FREQUENCY
  {0x1204, 0.1},  // MAX_FREQUENCY
  {0x1238, 0.01}, // JOG_FREQUENCY
  {0x1239, 1},    // JOG_DC_BRAKE_SETTING
  {0x1245, 1},    // DC_BRAKE_ENABLE
  {0x1246, 0.01}, // DC_BRAKE_FREQUENCY
  {0x1247, 0.1},  // DC_BRAKE_WAIT_TIME
  {0x1248, 1},    // DC_BRAKE_FORCE_DECEL
  {0x1249, 0.1},  // DC_BRAKE_TIME_DECEL
  {0x1278, 1},    // ACCELERATION_PROFILE_SWITCH_MODE
  {0x1104, 0.01}, // ACCELERATION_TIME_1
  {0x1106, 0.01}, // DECELERATION_TIME_1
  {0x1275, 0.01}, // ACCELERATION_TIME_2
  {0x1277, 0.01}, // DECELERATION_TIME_2
  {0x127D, 1},    // ACCELERATION_CURVE
  {0x127E, 1},    // DECELERATION_CURVE
  {0x1401, 1},    // INTELLIGENT_INPUT_1
  {0x1402, 1},    // INTELLIGENT_INPUT_2
  {0x1403, 1},    // INTELLIGENT_INPUT_3
};

enum class FunctionIndex
{
  INVERTER_STATUS,
  ROTATION_DIRECTION,
  OUTPUT_FREQUENCY,
  OUTPUT_CURRENT,
  ACTUAL_FREQUENCY,
  TORQUE,
  POWER,
  POSITION,
  KEYPAD_RUN_KEY_ROUTING,
  FREQUENCY_SOURCE_SETTING,
  RUN_COMMAND_SOURCE,
  BASE_FREQUENCY,
  MAX_FREQUENCY,
  JOG_FREQUENCY,
  JOG_DC_BRAKE_SETTING,
  DC_BRAKE_ENABLE,
  DC_BRAKE_FREQUENCY,
  DC_BRAKE_WAIT_TIME,
  DC_BRAKE_FORCE_DECEL,
  DC_BRAKE_TIME_DECEL,
  ACCELERATION_PROFILE_SWITCH_MODE,
  ACCELERATION_TIME_1,
  DECELERATION_TIME_1,
  ACCELERATION_TIME_2,
  DECELERATION_TIME_2,
  ACCELERATION_CURVE,
  DECELERATION_CURVE,
  INTELLIGENT_INPUT_1,
  INTELLIGENT_INPUT_2,
  INTELLIGENT_INPUT_3
};

constexpr Function get_function(FunctionIndex f)
{
  return FUNCTIONS[static_cast<std::int32_t>(f)];
}

void init()
{
  serial_port_config(1, RS485_MODE);

  modbus_client.begin(Port1, 9600);
}

std::int32_t read_modbus_function(FunctionIndex function)
{
  std::lock_guard<rtos_mutex> l(spi_bus_mutex);

  // From page 342 from the VFD manual:
  //   The PDU Register Number are addressed starting at zero. Therefore register
  //   numbered "1029h" are addressed as "1028h". Register address value
  //   (transmitted on Modbus line) is 1 less than the Register Number.
  return modbus_client.holdingRegisterRead(p1am::serial::modbus::MOTOR_CONTROLLER_ADDRESS, get_function(function).address - 1);
}

bool write_modbus_function(FunctionIndex function, std::int32_t value)
{
  std::lock_guard<rtos_mutex> l(spi_bus_mutex);

  // From page 342 from the VFD manual:
  //   The PDU Register Number are addressed starting at zero. Therefore register
  //   numbered "1029h" are addressed as "1028h". Register address value
  //   (transmitted on Modbus line) is 1 less than the Register Number.
  return modbus_client.holdingRegisterWrite(p1am::serial::modbus::MOTOR_CONTROLLER_ADDRESS, get_function(function).address - 1, value & 0xFFFF) != 0;
}

template<bool value, class T1, class T2>
using conditional_t = typename std::conditional<value, T1, T2>::type;

// Helper type for reading/writing scaled floating data where appropriate, and
// int32_t values otherwise. First template is the index of the value to read/write,
// and the second template is auto-calculated to either be float or int, depending on
// if the scaling factor for that function is 1.
template <FunctionIndex index_v,
          class TrueFalseHasScaleT = conditional_t<(std::int32_t(get_function(index_v).scale) != 1), std::true_type, std::false_type>>
struct modbus_helper;

template <FunctionIndex index_v> struct modbus_helper<index_v, std::true_type>
{
  static float read() { return read_modbus_function(index_v) * get_function(index_v).scale; }

  static bool write(float value)
  {
      return write_modbus_function(index_v, static_cast<std::int32_t>(value / get_function(index_v).scale));
  }
};

template <FunctionIndex index_v> struct modbus_helper<index_v, std::false_type>
{
  static std::int32_t read() { return read_modbus_function(index_v); }

  static bool write(std::int32_t value) { return write_modbus_function(index_v, value); }
};


bool MotorController::m_instance_exists = false;

MotorController::MotorController()
{
}

MotorController::~MotorController()
{
  if(m_valid)
  {
    m_instance_exists = false;
  }
}

// MotorController::MotorController(MotorController&& other)
// {
//   std::swap(other.m_valid, m_valid);
// }

// MotorController& MotorController::operator=(MotorController&& other)
// {
//   std::swap(other.m_valid, m_valid);
//   return *this;
// }

MotorController& MotorController::instance()
{
  static MotorController inst;
  inst.init();
  return inst;
}

bool MotorController::init()
{
  if(!m_instance_exists)
  {
    m_instance_exists = true;
    m_valid = true;

    motor_control::init();

    return true;
  }
  return false;
}

AccelerationProfileSwitchMode MotorController::getAccelerationProfileSwitchMode()
{
  return AccelerationProfileSwitchMode(modbus_helper<FunctionIndex::ACCELERATION_PROFILE_SWITCH_MODE>::read());
}

bool MotorController::setAccelerationProfileSwitchMode(AccelerationProfileSwitchMode value)
{
  ASSERT(value != AccelerationProfileSwitchMode::Error);
  return modbus_helper<FunctionIndex::ACCELERATION_PROFILE_SWITCH_MODE>::write(static_cast<std::int32_t>(value));
}

IntelligentInputSetting MotorController::getIntelligentInput1()
{
  return IntelligentInputSetting(modbus_helper<FunctionIndex::INTELLIGENT_INPUT_1>::read());
}

bool MotorController::setIntelligentInput1(IntelligentInputSetting value)
{
  ASSERT(value != IntelligentInputSetting::Error);
  return modbus_helper<FunctionIndex::INTELLIGENT_INPUT_1>::write(static_cast<std::int32_t>(value));
}

IntelligentInputSetting MotorController::getIntelligentInput2()
{
  return IntelligentInputSetting(modbus_helper<FunctionIndex::INTELLIGENT_INPUT_2>::read());
}

bool MotorController::setIntelligentInput2(IntelligentInputSetting value)
{
  ASSERT(value != IntelligentInputSetting::Error);
  return modbus_helper<FunctionIndex::INTELLIGENT_INPUT_2>::write(static_cast<std::int32_t>(value));
}

IntelligentInputSetting MotorController::getIntelligentInput3()
{
  return IntelligentInputSetting(modbus_helper<FunctionIndex::INTELLIGENT_INPUT_3>::read());
}

bool MotorController::setIntelligentInput3(IntelligentInputSetting value)
{
  ASSERT(value != IntelligentInputSetting::Error);
  return modbus_helper<FunctionIndex::INTELLIGENT_INPUT_3>::write(static_cast<std::int32_t>(value));
}

bool MotorController::setAccelerationTime1Sec(float value)
{
  return modbus_helper<FunctionIndex::ACCELERATION_TIME_1>::write(value);
}

float MotorController::getAccelerationTime1Sec()
{
  return modbus_helper<FunctionIndex::ACCELERATION_TIME_1>::read();
}

bool MotorController::setAccelerationTime2Sec(float value)
{
  return modbus_helper<FunctionIndex::ACCELERATION_TIME_2>::write(value);
}

float MotorController::getAccelerationTime2Sec()
{
  return modbus_helper<FunctionIndex::ACCELERATION_TIME_2>::read();
}

float MotorController::getDecelerationTime1Sec()
{
  return modbus_helper<FunctionIndex::DECELERATION_TIME_1>::read();
}

bool MotorController::setDecelerationTime1Sec(float value)
{
  return modbus_helper<FunctionIndex::DECELERATION_TIME_1>::write(value);
}

float MotorController::getDecelerationTime2Sec()
{
  return modbus_helper<FunctionIndex::DECELERATION_TIME_2>::read();
}

bool MotorController::setDecelerationTime2Sec(float value)
{
  return modbus_helper<FunctionIndex::DECELERATION_TIME_2>::write(value);
}

AccelerationCurve MotorController::getAccelerationCurve()
{
  return AccelerationCurve(modbus_helper<FunctionIndex::ACCELERATION_CURVE>::read());
}

bool MotorController::setAccelerationCurve(AccelerationCurve value)
{
  ASSERT(value != AccelerationCurve::Error);
  return modbus_helper<FunctionIndex::ACCELERATION_CURVE>::write(static_cast<std::int32_t>(value));
}

AccelerationCurve MotorController::getDecelerationCurve()
{
  return AccelerationCurve(modbus_helper<FunctionIndex::DECELERATION_CURVE>::read());
}

bool MotorController::setDecelerationCurve(AccelerationCurve value)
{
  ASSERT(value != AccelerationCurve::Error);
  return modbus_helper<FunctionIndex::DECELERATION_CURVE>::write(static_cast<std::int32_t>(value));
}

InverterStatus MotorController::getInverterStatus()
{
  return InverterStatus(modbus_helper<FunctionIndex::INVERTER_STATUS>::read());
}

FrequencySource MotorController::getFrequencySource()
{
  return FrequencySource(modbus_helper<FunctionIndex::FREQUENCY_SOURCE_SETTING>::read());
}

bool MotorController::setFrequencySource(FrequencySource value)
{
  ASSERT(value != FrequencySource::Error);
  return modbus_helper<FunctionIndex::FREQUENCY_SOURCE_SETTING>::write(static_cast<std::int32_t>(value));
}

float MotorController::getOutputFrequencyHz()
{
  return modbus_helper<FunctionIndex::OUTPUT_FREQUENCY>::read();
}

bool MotorController::setOutputFrequencyHz(float value)
{
  return modbus_helper<FunctionIndex::OUTPUT_FREQUENCY>::write(value);
}

float MotorController::getActualFrequencyHz()
{
  return modbus_helper<FunctionIndex::ACTUAL_FREQUENCY>::read();
}

float MotorController::getBaseFrequencyHz()
{
  return modbus_helper<FunctionIndex::BASE_FREQUENCY>::read();
}

bool MotorController::setBaseFrequencyHz(float value)
{
  return modbus_helper<FunctionIndex::BASE_FREQUENCY>::write(value);
}

float MotorController::getMaxFrequencyHz()
{
  return modbus_helper<FunctionIndex::MAX_FREQUENCY>::read();
}

bool MotorController::setMaxFrequencyHz(float value)
{
  return modbus_helper<FunctionIndex::MAX_FREQUENCY>::write(value);
}

float MotorController::getJogFrequencyHz()
{
  return modbus_helper<FunctionIndex::JOG_FREQUENCY>::read();
}

bool MotorController::setJogFrequencyHz(float value)
{
  return modbus_helper<FunctionIndex::JOG_FREQUENCY>::write(value);
}

JogStopMode MotorController::getJogStopMode()
{
  return JogStopMode(modbus_helper<FunctionIndex::JOG_DC_BRAKE_SETTING>::read());
}

bool MotorController::setJogStopMode(JogStopMode value)
{
  ASSERT(value != JogStopMode::Error);
  return modbus_helper<FunctionIndex::JOG_DC_BRAKE_SETTING>::write(static_cast<std::int32_t>(value));
}

float MotorController::getOutputCurrentAmps()
{
  return modbus_helper<FunctionIndex::OUTPUT_CURRENT>::read();
}

RotationDirection MotorController::getRotationDirection()
{
  return RotationDirection(modbus_helper<FunctionIndex::ROTATION_DIRECTION>::read());
}

std::int32_t MotorController::getTorquePercent()
{
  return modbus_helper<FunctionIndex::TORQUE>::read();
}

float MotorController::getPowerKw()
{
  return modbus_helper<FunctionIndex::POWER>::read();
}

std::int32_t MotorController::getPositionFeedback()
{
  return modbus_helper<FunctionIndex::POSITION>::read();
}

RunKeyRouting MotorController::getKeypadRunKeyRouting()
{
  return RunKeyRouting(modbus_helper<FunctionIndex::KEYPAD_RUN_KEY_ROUTING>::read());
}

bool MotorController::setKeypadRunKeyRouting(RunKeyRouting value)
{
  ASSERT(value != RunKeyRouting::Error);
  return modbus_helper<FunctionIndex::KEYPAD_RUN_KEY_ROUTING>::write(static_cast<std::int32_t>(value));
}

RunCommandSource MotorController::getRunCommandSource()
{
  return RunCommandSource(modbus_helper<FunctionIndex::RUN_COMMAND_SOURCE>::read());
}

bool MotorController::setRunCommandSource(RunCommandSource value)
{
  ASSERT(value != RunCommandSource::Error);
  return modbus_helper<FunctionIndex::RUN_COMMAND_SOURCE>::write(static_cast<std::int32_t>(value));
}

DcBrakeMode MotorController::getDcBrakeMode()
{
  return DcBrakeMode(modbus_helper<FunctionIndex::DC_BRAKE_ENABLE>::read());
}

bool MotorController::setDcBrakeMode(DcBrakeMode value)
{
  ASSERT(value != DcBrakeMode::Error);
  return modbus_helper<FunctionIndex::DC_BRAKE_ENABLE>::write(static_cast<std::int32_t>(value));
}

float MotorController::getDcBrakeFrequencyHz()
{
  return modbus_helper<FunctionIndex::DC_BRAKE_FREQUENCY>::read();
}

bool MotorController::setDcBrakeFrequencyHz(float value)
{
  return modbus_helper<FunctionIndex::DC_BRAKE_FREQUENCY>::write(value);
}

float MotorController::getDcBrakeWaitTimeSec()
{
  return modbus_helper<FunctionIndex::DC_BRAKE_WAIT_TIME>::read();
}

bool MotorController::setDcBrakeWaitTimeSec(float value)
{
  return modbus_helper<FunctionIndex::DC_BRAKE_WAIT_TIME>::write(value);
}

std::int32_t MotorController::getDcBrakeDecelerationForcePercentage()
{
  return modbus_helper<FunctionIndex::DC_BRAKE_FORCE_DECEL>::read();
}

bool MotorController::setDcBrakeDecelerationForcePercentage(std::int32_t value)
{
  return modbus_helper<FunctionIndex::DC_BRAKE_FORCE_DECEL>::write(value);
}

float MotorController::getDcBrakeDecelerationTimeSec()
{
  return modbus_helper<FunctionIndex::DC_BRAKE_TIME_DECEL>::read();
}

bool MotorController::setDcBrakeDecelerationTimeSec(float value)
{
  return modbus_helper<FunctionIndex::DC_BRAKE_TIME_DECEL>::write(value);
}

}
