#pragma once

namespace ride {

const float MIN_RUN_DISTANCE_M = 5.0; // ~16 feet
const float MAX_RUN_DISTANCE_M = 25.0; // ~82 feet
const float MIN_PULLEY_DIAMETER = 0.195; // ~8 inches
const float MAX_PULLEY_DIAMETER = 0.215;

// Directly attached to motor axle
const float MOTOR_PULLEY_DIAMETER_M = 0.0533; // 2.1 inches
// On belt from motor
const float BELTED_PULLEY_DIAMETER_M = 0.14478; // 5.7 inches

const float DEFAULT_PULLEY_DIAMETER_M = 0.2032;
const float DEFAULT_MOTOR_TO_PULLEY_RATIO = 0.36842;
  
// =============================================================================
// Models the geometry
// A picture
//   |                             G                         |
//      A B  C   D                    E                    F
//   |  | |    |   |                                 |       |
// H O==^**=====###==========================================O
//   |
//[]-o  I
class Geometry {
public:
  Geometry( float total_length_m, float buffer_length_m, float return_switch_position_m,
    float return_trigger_length_m, float trigger_to_carriage_m, float carriage_length_m,
    float pulley_diameter_m, float motor_to_pulley_ratio); 
  static Geometry default_geometry();

  // Target expected distance of travel after punching go until stopping movement
  float total_length_m() const { return _total_length_m; }
  float buffer_length_m() const { return _buffer_length_m; }
  float return_switch_position_m() const { return _return_trigger_length_m; }

  float return_trigger_length_m() const { return _return_trigger_length_m; }
  float trigger_to_carriage_m() const { return _trigger_to_carriage_m; }
  float carriage_length_m() const { return _carriage_length_m; }

  // Diameter of the pulley driving the acceleration loop in meters
  float pulley_diameter_m() const { return _pulley_diameter_m; }
  // Number of times the motor spins before turning the pulley once
  float motor_to_pulley_ratio() const { return _motor_to_pulley_ratio; }

  float pulley_circumference_m() const { return _pulley_circumference_m; }
  float run_distance_m() const { return _run_distance_m; }

  // In order to travel the intended distance with a driving pulley of a certain diameter
  // it implies a certain number of rotations (assuming no slippage).
  float pulley_revolutions_per_run() const;
  float motor_revolutions_per_run() const;

private: 
  float calculate_run_distance() const;

  // Fixed geometry
  float _total_length_m; // G
  float _buffer_length_m; // F
  float _return_switch_position_m; //A
  // On cable, relative to return trigger.
  float _return_trigger_length_m; // B
  float _trigger_to_carriage_m; // C
  float _carriage_length_m; // D
  // Geometry of engine/pulley mounting
  float _pulley_diameter_m;
  float _motor_to_pulley_ratio;

  // Calculated
  float _pulley_circumference_m;
  float _run_distance_m;
};

Geometry::Geometry(
    float total_length_m,
    float buffer_length_m,
    float return_switch_position_m,

    float return_trigger_length_m,
    float trigger_to_carriage_m,
    float carriage_length_m,

    float pulley_diameter_m = DEFAULT_PULLEY_DIAMETER_M,
    float motor_to_pulley_ratio = DEFAULT_MOTOR_TO_PULLEY_RATIO)
  :
  _total_length_m(total_length_m),
  _buffer_length_m(buffer_length_m),
  _return_switch_position_m(return_switch_position_m),

  _return_trigger_length_m(return_trigger_length_m),
  _trigger_to_carriage_m(trigger_to_carriage_m),
  _carriage_length_m(carriage_length_m),

  _pulley_diameter_m(pulley_diameter_m),
  _motor_to_pulley_ratio(motor_to_pulley_ratio)
{
  _pulley_circumference_m = _pulley_diameter_m * M_PI;
  //ASSERT(_pulley_circumference_m > 0.2);
  //ASSERT(_run_distance_m < 30);
  _run_distance_m = calculate_run_distance();
  // Make sure we didn't enter some stupid numbers
  //ASSERT(_run_distance_m > 5);
  //ASSERT(_run_distance_m < 30);
}

// TODO: Let's make these the canonical measurements
// Currently just guesses
Geometry Geometry::default_geometry() {
  return Geometry(
    32.308, // Total Length (107ft) [Measured]

    // Buffer Length (13.1 ft) [Experimentally determined]
    4.0,
    0.4826, // Return switch position (19in) [Measured]

    0.3048, // Return trigger length (12in) [Measured]
    5.334, // Return trigger to carriage (17.5ft) [Measured]
    0.6223 // Carriage length (24.5in) [Measured]
  );
}

float Geometry::pulley_revolutions_per_run() const {
  return _run_distance_m / _pulley_circumference_m;
}

float Geometry::motor_revolutions_per_run() const {
  return _run_distance_m / _pulley_circumference_m / _motor_to_pulley_ratio;
}

float Geometry::calculate_run_distance() const {
  return _total_length_m
    - _return_trigger_length_m
    - (_return_trigger_length_m + _trigger_to_carriage_m + _carriage_length_m)
    - _buffer_length_m;
}

}
