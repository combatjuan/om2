#pragma once

#include <utility>

namespace state_machine
{
  enum class State
  {
    Init,
    NotReady,
    Ready,
    Preflight,
    Forward,
    Stopping,
    Postflight,
    Complete,
    ResetFast,
    ResetSlow,
    TimedOut,
    Configuring,
  };

  enum class Event
  {
    Idle,
    InitComplete,
    LimitSwitchRising,
    LimitSwitchFalling,
    EngageButtonRising,
    EngageButtonFalling,
    GoButton,

    // Enters configuration, Exits configuration to ready
    ConfigureButtonWithLimitSwitch,

    // Enters configuration, Exits configuration to not ready
    ConfigureButtonWithoutLimitSwitch,
    Timeout,
    TargetDistanceReached,
  };

  // Returns the state after an event occurs
  State update(State current_state, Event current_event);
}