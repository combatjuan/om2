#pragma once

#include <FreeRTOS_SAMD21.h>

#include "peripherals.h"

// This must be defined elsewhere, exactly once. It executes when
// an assertion fails before we drop into blinking lights
extern void on_assert_fail();

#define _STR(x) #x
#define STR(x) _STR(x)

#define ASSERT(condition)                                       \
    if (!(condition))                                           \
    {                                                           \
        Serial.println("=================");                    \
        Serial.println("Assertion Failed!");                    \
        Serial.println("Condition:\t" STR(condition));          \
        Serial.println("File:\t\t" STR(__FILE__));              \
        Serial.println("Line:\t\t" STR(__LINE__));              \
                                                                \
        on_assert_fail();                                       \
    }
