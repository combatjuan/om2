#pragma once

#include "thread.h"

// Invoke some function regularly on a timeout
//
// \tparam Predicate - An invokable object with signature (bool(*)())
template<class Predicate, class Rep, class Period>
void recurring(std::chrono::duration<Rep, Period> interval, Predicate&& p)
{
  TickType_t tick_history = xTaskGetTickCount();
  while(p())
  {
    recurring_sleep_for(tick_history, interval);
  }
}
