#include <chrono>
#include <iostream>

#include "ride.h"
#include "ride_geometry.h"

using namespace std::chrono;
using std::cout;
using std::endl;

// Inquiry: Is this the world's crummiest unit-testing framework?
// Response: Yes.
#define TEST(F) \
  if (F()) { cout << "PASSED: " << #F << endl; }\
  else { cout << "FAILED: " << #F << endl; }

// -----------------------------------------------------------------------------
// Helpers
bool is_approximately(float a, float b, float tolerance = 0.001) {
  return (a * (1.0 - tolerance) < b)
    && (a * (1.0 + tolerance) > b);
}

template<class Rep, class Period>
std::ostream& operator << (std::ostream& out, std::chrono::duration<Rep, Period> d)
{
    return out << std::chrono::duration_cast<std::chrono::duration<double>>(d).count() << "s";
}

void print_configuration(const ride::Configuration& conf) {
  cout << "Configuration:\n"
    << "  Acceleration G: " << conf.acceleration_gs() << "\n"
    << "  Acceleration s: " << conf.acceleration_time_s() << "s\n"
    << "  Acceleration m: " << conf.acceleration_distance_m() << "m"
      << "(" << ride::m_to_ft(conf.acceleration_distance_m()) << "ft)\n"
    << "  Deceleration G: " << conf.deceleration_gs() << "\n"
    << "  Deceleration s: " << conf.deceleration_time_s() << "s\n"
    << "  Deceleration m: " << conf.deceleration_distance_m() << "m"
      << "(" << ride::m_to_ft(conf.deceleration_distance_m()) << "ft)\n"
    << "  Max Speed mps:  " << conf.max_speed_mps() << " (" << ride::mps_to_mph(conf.max_speed_mps()) << "mph)" << endl;
}

// -----------------------------------------------------------------------------
// Tests
bool test_configurations() {
  ride::Geometry geo = ride::Geometry::default_geometry();
  ride::Configuration conf = ride::Configuration(geo.run_distance_m(), 0.2);
  print_configuration(conf);
  return is_approximately(conf.max_speed_mps(), 8.38);
}

bool test_default_geometry() {
  ride::Geometry geo = ride::Geometry::default_geometry();
  cout << "Total ride distance: " << geo.run_distance_m() << "m" << endl;
  float pulley_revolutions = geo.pulley_revolutions_per_run();
  float motor_revolutions = geo.motor_revolutions_per_run();
  return is_approximately(pulley_revolutions, 33.679)
    && is_approximately(motor_revolutions, 91.415888);
}

// -----------------------------------------------------------------------------
int main(int, char**) {
  TEST(test_configurations);
  TEST(test_default_geometry);
  return 0;
}
