#include <chrono>
#include <iomanip>
#include <iostream>

#include "ride.h"
#include "ride_geometry.h"

using namespace std::chrono;
using std::cout;
using std::endl;

// -----------------------------------------------------------------------------
// Helpers
template<class Rep, class Period>
std::ostream& operator << (std::ostream& out, std::chrono::duration<Rep, Period> d)
{
    return out << std::chrono::duration_cast<std::chrono::duration<double>>(d).count() << "s";
}

void print_configuration(const ride::Configuration& conf) {
  cout << "Configuration:\n"
    << "  Acceleration G: " << conf.acceleration_gs() << "\n"
    << "  Acceleration s: " << conf.acceleration_time_s() << "s\n"
    << "  Acceleration m: " << conf.acceleration_distance_m() << "m"
      << "(" << ride::m_to_ft(conf.acceleration_distance_m()) << "ft)\n"
    << "  Deceleration G: " << conf.deceleration_gs() << "\n"
    << "  Deceleration s: " << conf.deceleration_time_s() << "s\n"
    << "  Deceleration m: " << conf.deceleration_distance_m() << "m"
      << "(" << ride::m_to_ft(conf.deceleration_distance_m()) << "ft)\n"
    << "  Max Speed mps:  " << conf.max_speed_mps() << " (" << ride::mps_to_mph(conf.max_speed_mps()) << "mph)" << endl;
}

void print_header() {
  cout << "      Acc(Gs) Speed(m/s)  Speed(mph)   AccD(m)     AccD(ft)      AccT(s)      DecD(m)      DecD(ft)       DecT(s)     TotalD(m)   TotalD(ft)   TotalD(rot)   TotalT(s)   ResetSlow(s)   ResetFast(s)" << endl;
}

using ride::mps_to_mph;
using ride::m_to_ft;

void print_line(const ride::Configuration& conf) {
  const auto rotations_travel = (conf.acceleration_distance_m() + conf.deceleration_distance_m()) / ride::DEFAULT_PULLEY_DIAMETER_M;

  cout << std::fixed << std::setfill(' ') << std::setprecision(4) 
    << std::setw(11) << conf.acceleration_gs() << "Gs"
    << std::setw(11) << conf.max_speed_mps()
    << std::setw(11) << mps_to_mph(conf.max_speed_mps())

    << std::setw(11) << conf.acceleration_distance_m() << "  "
    << std::setw(11) << m_to_ft(conf.acceleration_distance_m()) << "  "
    << std::setw(11) << conf.acceleration_time_s() << "  "

    << std::setw(11) << conf.deceleration_distance_m() << "  "
    << std::setw(11) << m_to_ft(conf.deceleration_distance_m()) << "  "
    << std::setw(11) << conf.deceleration_time_s() << "  "

    << std::setw(11) << conf.acceleration_distance_m() + conf.deceleration_distance_m() << "  "
    << std::setw(11) << m_to_ft(conf.acceleration_distance_m() + conf.deceleration_distance_m()) << "  "
    << std::setw(11) << rotations_travel << "rot  "
    << std::setw(11) << conf.acceleration_time_s() + conf.deceleration_time_s() << "  "

    << std::setw(11) << ride::expected_travel_time(RESET_FAST_MOTOR_HZ, rotations_travel * HIGH_SPEED_REVERSE_PERCENT) << "  "
    << std::setw(11) << ride::expected_travel_time(RESET_SLOW_MOTOR_HZ, rotations_travel * (1 - HIGH_SPEED_REVERSE_PERCENT)) << "  "
    << endl;
}

// -----------------------------------------------------------------------------
int main(int, char**) {
  ride::Geometry geo = ride::Geometry::default_geometry();
  print_header();
  for (float a = 0.01; a <= 0.5; a += 0.01) {
    ride::Configuration conf = ride::Configuration(geo.run_distance_m(), a);
    print_line(conf);
  }
  return 0;
}
