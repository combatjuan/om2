#include <FreeRTOS_SAMD21.h>
#include <P1AM.h>
#include <Ethernet.h>

// The base board defines min and max macros that don't jive
// with the standard library min/max functions
#undef min
#undef max

#include "allocate.h"
#include "thread.h"
#include "recurring.h"
#include "constants.h"
#include "state_machine.h"
#include "peripherals.h"
#include "assert.h"
#include "motor_control.h"
#include "stats.h"
#include "ride.h"

#include <iterator>
#include <array>
#include <chrono>
#include <tuple>
#include <algorithm>
#include <type_traits>
#include <cstring>
#include <mutex>

using namespace std::chrono;

rtos_mutex spi_bus_mutex;

namespace
{
TaskHandle_t tid_main;
const IPAddress ip_addr(p1am::eth::IP_ADDR[0], p1am::eth::IP_ADDR[1], p1am::eth::IP_ADDR[2], p1am::eth::IP_ADDR[3]);
EthernetUDP udp_client;

bool partner_ip_addr_set = false;
IPAddress partner_ip_addr;

std::size_t current_config_index = 0;
const ride::Geometry current_geometry = ride::Geometry::default_geometry();
ride::Configuration current_configuration(0, 0, 0);

// Calculated any time we update the configuration

float target_output_hz = 0;

// This is the count we expect to hit on the encoder before we cut power,
// which is # of rotations * 10
std::int32_t target_forward_counts = 0;

// These gets updated when we start reversing to be some percent of the distance
// that was traveled forward - it is set to the expected encoder position where
// we should stop reversing quickly (assuming the encoder position starts
// counting down when we drive)
std::int32_t target_reverse_counts = 0;
duration<float> reset_fast_timeout{0};
duration<float> reset_slow_timeout{0};

duration<float> forward_timeout{0};
duration<float> stopping_timeout{0};
}

// Set all the default settings for the motor controller (and other things)
void setDefaultConfigurations();

// Update the current config index and parameters, and send
// all the settings to the motor
void setConfigIndex(std::size_t index);

// Sets the light blinking speed based on the currently selected
// confiuration index
void setBlinkSpeedFromConfigIndex();

duration<float> calculateTimeout(float run_speed_hz, float target_rotations);

// Can't be called main() - that's still a special
// function name in arduino land
void program(void*);

// Called before we drop into the blinking lights
// loop
void on_assert_fail()
{
  peripherals::setMotor(peripherals::MotorState::Stop);
  peripherals::setLaunchSprinkler(false);
  peripherals::setSlideSprinkler(false);

  vTaskSuspendAll();

  while (true)
  {
      delay(1000);
      peripherals::setLights(true, false);
      delay(1000);
      peripherals::setLights(false, true);
  }
}

void setup() {
  // Initialize serial communication at 115200 bits per second
  Serial.begin(115200);

  peripherals::init();
  stats::init();
  // while (!Serial) {
  //   ;  //Wait for Modules to Sign on
  // }

  EthernetClass::begin(const_cast<std::uint8_t*>(p1am::eth::MAC_ADDR.data()), ip_addr);
  ASSERT(EthernetClass::hardwareStatus() != EthernetHardwareStatus::EthernetNoHardware);

  setDefaultConfigurations();
  setConfigIndex(0);

  udp_client.begin(p1am::eth::UDP_PORT_IN);

  ASSERT(MotorController::instance());

  Serial.println("====== HI! =======");

  pinMode(p1am::LED_PIN, OUTPUT);
  pinMode(p1am::SWITCH_PIN, INPUT);

  // Tell FreeRTOS to set the LED pin to high to light it up, so it
  // can be used as a status
  vSetErrorLed(p1am::LED_PIN, true);

  // Tell FreeRTOS to dump state to Serial device on error
  vSetErrorSerial(&Serial);

  // Main loop has relatively high priority - 5 more than idle
  xTaskCreate(&program, "main()", 256, nullptr, tskIDLE_PRIORITY+priority::MAIN_LOOP, &tid_main);

  // Start the RTOS, this function will never return and will schedule the tasks.
  Serial.println("Starting up the scheduler...");
  vTaskStartScheduler();

  while(1)
  {
	  Serial.println("We should never see this (TM)!");
	  Serial.flush();
	  delay(1000);
  }
}

// FreeRTOS configures `loop()` to be the idle function; so don't do
// anything that will block it. But we _can_ use `delay()` within,
// because it is interrupt-friendly, so the interrupt clock signal
// can tick the RTOS
void loop() {
}

void setDefaultConfigurations()
{
  Serial.println("Setting default motor controller settings...");

  auto& controller = MotorController::instance();

  ASSERT(controller.setFrequencySource(config::defaults::FREQUENCY_SOURCE));

  // Max and base frequency are dependent on each other - if base is too high, we
  // cant bring max lower; and if max is too low, we can't bring base higher.
  //
  // Rather than do logic and solve this right, we're just setting them both twice,
  // and the second time should succeed, provided out constants are valid settings
  controller.setMaxFrequencyHz(config::defaults::MAX_FREQUENCY);
  controller.setBaseFrequencyHz(config::defaults::BASE_FREQUENCY);
  ASSERT(controller.setMaxFrequencyHz(config::defaults::MAX_FREQUENCY));
  ASSERT(controller.setBaseFrequencyHz(config::defaults::BASE_FREQUENCY));

  ASSERT(controller.setIntelligentInput1(config::defaults::INTELLIGENT_INPUT_1));
  ASSERT(controller.setIntelligentInput2(config::defaults::INTELLIGENT_INPUT_2));
  ASSERT(controller.setIntelligentInput3(config::defaults::INTELLIGENT_INPUT_3));
  ASSERT(controller.setJogFrequencyHz(config::defaults::JOG_FREQUENCY));
  ASSERT(controller.setOutputFrequencyHz(config::defaults::OUTPUT_FREQUENCY_HZ));
  ASSERT(controller.setAccelerationTime1Sec(config::defaults::ACCELERATION_TIME_1_S));
  ASSERT(controller.setDecelerationTime1Sec(config::defaults::DECELERATION_TIME_1_S));
  ASSERT(controller.setAccelerationTime2Sec(config::defaults::ACCELERATION_TIME_2_S));
  ASSERT(controller.setDecelerationTime2Sec(config::defaults::DECELERATION_TIME_2_S));
  ASSERT(controller.setAccelerationProfileSwitchMode(config::defaults::ACCELERATION_PROFILE_SWITCH_MODE));
  ASSERT(controller.setAccelerationCurve(config::defaults::ACCELERATION_CURVE));
  ASSERT(controller.setDecelerationCurve(config::defaults::DECELERATION_CURVE));
  ASSERT(controller.setKeypadRunKeyRouting(config::defaults::KEYPAD_RUN_ROUTING));
  ASSERT(controller.setRunCommandSource(config::defaults::RUN_COMMAND_SOURCE));
  ASSERT(controller.setJogStopMode(config::defaults::JOG_STOP_MODE));
  ASSERT(controller.setDcBrakeMode(config::defaults::DC_BRAKE_MODE));
  ASSERT(controller.setDcBrakeFrequencyHz(config::defaults::DC_BRAKE_FREQUENCY_HZ));
  ASSERT(controller.setDcBrakeWaitTimeSec(config::defaults::DC_BRAKE_WAIT_TIME_S));
  ASSERT(controller.setDcBrakeDecelerationForcePercentage(config::defaults::DC_BRAKE_FORCE_DECELERATION_PCT));
  ASSERT(controller.setDcBrakeDecelerationTimeSec(config::defaults::DC_BRAKE_TIME_DECELERATION_S));
}

void setBlinkSpeedFromConfigIndex()
{
  peripherals::setLightBlinkInterval(microseconds(seconds(1)) / ((current_config_index+1)*(current_config_index+1)));
}

void setConfigIndex(std::size_t index)
{
  ASSERT(index < config::PRESETS.size());

  Serial.println("===============");
  Serial.print("Setting config ");
  Serial.println(index);

  current_config_index = index;
  current_configuration = ride::Configuration(current_geometry.run_distance_m(), config::PRESETS[index].accel_gs);

  setBlinkSpeedFromConfigIndex();

  // t_m/t_s = target m/s
  //
  // p_m = drive line pully circumference meters
  // m_m = motor pully circumference meters
  //
  // m_hz = motor hz
  // m_rpm = motor target rpm
  // m_hzrpm = moter hz/rpm
  // p_rpm = pully target rpm
  //
  // p_rpm * p_m = m_rpm * m_m -> p_rpm = m_rpm * m_m/p_m
  // m_hz = m_rpm * m_hzrpm
  // t_m/t_s = p_rpm * p_m / 60s
  // p_rpm = t_m/t_s / (p_m / 60s) = t_m/t_s * 60s / p_m
  //
  // m_rpm = p_rpm / (p_m/m_m) = p_rpm * p_m/m_m
  // m_hz = p_rpm * p_m/m_m * m_hzrpm
  // m_hz = t_m/t_s * 60s / p_m * p_m/m_m * m_hzrpm

  target_output_hz = config::MOTOR_HZ_PER_RPM * (current_configuration.max_speed_mps() / current_geometry.pulley_circumference_m() * 60 * 1./current_geometry.motor_to_pulley_ratio());
  target_forward_counts = static_cast<std::int32_t>(current_configuration.acceleration_distance_m() / current_geometry.pulley_circumference_m() * 10);

  // Inclue a small buffer on the forward timeout because we _want_ to stop based on encoder counts
  forward_timeout = current_configuration.acceleration_time_s() * CALCULATED_TIMEOUT_RATIO;

   // Don't use the ratio here because this isn't a safety, it's just used to know when to move on
  stopping_timeout = current_configuration.deceleration_time_s();

  auto& controller = MotorController::instance();

  Serial.print("Target Gs: ");
  Serial.println(config::PRESETS[index].accel_gs);

  Serial.print("Target speed M/S: ");
  Serial.println(current_configuration.max_speed_mps());

  Serial.print("Output Frequency: ");
  Serial.println(target_output_hz);
  ASSERT(controller.setOutputFrequencyHz(target_output_hz));

  Serial.print("Forward Acceleration Time: ");
  Serial.println(current_configuration.acceleration_time_s().count());
  ASSERT(controller.setAccelerationTime1Sec(current_configuration.acceleration_time_s().count()));

  Serial.print("Forward Acceleration Timeout: ");
  Serial.println(forward_timeout.count());

  Serial.print("Foward Deceleration Time: ");
  Serial.println(current_configuration.deceleration_time_s().count());
  ASSERT(controller.setDecelerationTime1Sec(current_configuration.deceleration_time_s().count()));

  Serial.print("Target Forward Encoder Clicks: ");
  Serial.println(target_forward_counts);

  Serial.println("===============");
}

void stopCollectingAndOutputStats();

// Read a UDP packet if any are buffered, and react. "set" packets are
// ignored unless allow_configuration is set
void handleUdpPacket(bool allow_configuration);

// Compare the current stored state information and the new state information, then
// generate the event to feed into the state machine and update the stored state
//
// \note Since we can only generate one event per tick, this will only store state
// information related to the event generated, allowing another simultaneous event
// to be detected on the next tick (provided the appropriate state persists)
state_machine::Event observeNewSystemState(state_machine::State current_state, peripherals::SystemState& stored_system_state, const peripherals::SystemState new_system_state);

// Check if the appropriate timer for the current state has timed out and
// return either the Idle or Timeout event
state_machine::Event checkTimers(rtos_clock::time_point state_entry_time);

void onEnterNewState(state_machine::State new_state, peripherals::SystemState current_system_state);
void onRemainInState(state_machine::State current_state, peripherals::SystemState current_system_state);

void onRemainInNotReady(peripherals::SystemState current_state);
void onRemainInConfiguring(peripherals::SystemState current_state);
void onRemainInForward(peripherals::SystemState current_state);
void onRemainInResetFast(peripherals::SystemState current_state);
void onRemainInResetSlow(peripherals::SystemState current_state);

void onEnterInit();
void onEnterNotReady();
void onEnterReady();
void onEnterPreflight();
void onEnterForward();
void onEnterStopping();
void onEnterPostFlight();
void onEnterComplete();
void onEnterResetFast(peripherals::SystemState current_system_state);
void onEnterResetSlow();
void onEnterConfiguring();

void program(void*)
{
  state_machine::State current_state = state_machine::State::Init;
  peripherals::SystemState current_system_state;
  auto state_entry = rtos_clock::now();

  auto avg_rate_start = rtos_clock::now();
  std::size_t loop_count = 0;

  onEnterInit();

  recurring(microseconds(seconds(1))/MAIN_LOOP_RATE_HZ,
    [&]{
        if(PRINT_AVERAGE_LOOP_RATE)
        {
          const auto now = rtos_clock::now();
          const auto time_elapsed = now - avg_rate_start;
          if(time_elapsed > seconds(5))
          {
            float avg_rate = loop_count / duration_cast<duration<float>>(time_elapsed).count();
            Serial.print("Average loops/s: ");
            Serial.println(avg_rate);

            loop_count = 0;
            avg_rate_start = now;
          }
          loop_count++;
        }

        const auto new_system_state = peripherals::readSystemState();

        auto current_event = observeNewSystemState(current_state, current_system_state, new_system_state);

        stats::current_encoder_position = current_system_state.encoder_position;

        // If nothing else happened, update timers
        if(current_event == state_machine::Event::Idle)
        {
          current_event = checkTimers(current_state, state_entry);
        }

        const auto next_state = state_machine::update(current_state, current_event);
        const bool did_state_transition = next_state != current_state;

        if(did_state_transition)
        {
          state_entry = rtos_clock::now();
          onEnterNewState(next_state, current_system_state);
        }
        else
        {
          onRemainInState(next_state, current_system_state);
        }

        handleUdpPacket(current_state == state_machine::State::Configuring);

        current_state = next_state;

        return true;
    }
  );
}

state_machine::Event observeNewSystemState(state_machine::State current_state, peripherals::SystemState& stored_system_state, const peripherals::SystemState new_system_state)
{
  // Always just copy all the information that isn't used to make state transitions
  stored_system_state.jog_left = new_system_state.jog_left;
  stored_system_state.jog_right = new_system_state.jog_right;
  stored_system_state.encoder_position = new_system_state.encoder_position;

  // If we're running the motor to some distance, check if we've reached that
  if(current_state == state_machine::State::Forward && stored_system_state.encoder_position >= target_forward_counts)
  {
    return state_machine::Event::TargetDistanceReached;
  }

  if(current_state == state_machine::State::ResetFast && stored_system_state.encoder_position <= target_reverse_counts)
  {
    return state_machine::Event::TargetDistanceReached;
  }

  // Limit switch is highest priority switch, because it's used to stop the reset operation
  const auto limit_switch_rising = !stored_system_state.limit_switch && new_system_state.limit_switch;
  const auto limit_switch_falling = stored_system_state.limit_switch && !new_system_state.limit_switch;

  stored_system_state.limit_switch = new_system_state.limit_switch;
  if(limit_switch_rising)
  {
    return state_machine::Event::LimitSwitchRising;
  }

  if(limit_switch_falling)
  {
    return state_machine::Event::LimitSwitchFalling;
  }

  const auto go_button = !stored_system_state.go_button && new_system_state.go_button;

  stored_system_state.go_button = new_system_state.go_button;
  if(go_button)
  {
    return state_machine::Event::GoButton;
  }

  const auto engage_button_rising = !stored_system_state.engage_button && new_system_state.engage_button;
  const auto engage_button_falling = stored_system_state.engage_button && !new_system_state.engage_button;

  stored_system_state.engage_button = new_system_state.engage_button;
  if(engage_button_rising)
  {
    return state_machine::Event::EngageButtonRising;
  }

  if(engage_button_falling)
  {
    return state_machine::Event::EngageButtonFalling;
  }

  const auto configure_button = !stored_system_state.configure_button && new_system_state.configure_button;

  stored_system_state.configure_button = new_system_state.configure_button;
  if(configure_button)
  {
    if(stored_system_state.limit_switch)
    {
      return state_machine::Event::ConfigureButtonWithLimitSwitch;
    }
    else
    {
      return state_machine::Event::ConfigureButtonWithoutLimitSwitch;
    }
  }

  return state_machine::Event::Idle;
}

state_machine::Event checkTimers(state_machine::State current_state, rtos_clock::time_point state_entry)
{
  const auto time_since_state_entry = rtos_clock::now() - state_entry;

  if(current_state == state_machine::State::Forward && time_since_state_entry > forward_timeout)
  {
    Serial.println("Foward timed out!");
    return state_machine::Event::Timeout;
  }

  if(current_state == state_machine::State::ResetSlow && time_since_state_entry > reset_slow_timeout)
  {
    Serial.println("Slow reset timed out!");
    return state_machine::Event::Timeout;
  }

  if(current_state == state_machine::State::ResetFast && time_since_state_entry > reset_fast_timeout)
  {
    Serial.println("Fast reset timed out!");
    return state_machine::Event::Timeout;
  }

  if(current_state == state_machine::State::Stopping && time_since_state_entry > stopping_timeout)
  {
    Serial.println("Stopping Complete!");
    return state_machine::Event::Timeout;
  }

  if(current_state == state_machine::State::Postflight && time_since_state_entry > TIMEOUT_POST_FLIGHT)
  {
    Serial.println("Postflight Complete!");
    return state_machine::Event::Timeout;
  }

  if(current_state == state_machine::State::Init && time_since_state_entry > TIMEOUT_INIT)
  {
    return state_machine::Event::InitComplete;
  }

  return state_machine::Event::Idle;
}

void onEnterNewState(state_machine::State new_state, peripherals::SystemState current_system_state)
{
  switch(new_state)
  {
    case state_machine::State::NotReady:
      return onEnterNotReady();
    case state_machine::State::Ready:
      return onEnterReady();
    case state_machine::State::Preflight:
      return onEnterPreflight();
    case state_machine::State::Forward:
      return onEnterForward();
    case state_machine::State::Stopping:
      return onEnterStopping();
    case state_machine::State::Postflight:
      return onEnterPostflight();
    case state_machine::State::Complete:
      return onEnterComplete();
    case state_machine::State::ResetFast:
      return onEnterResetFast(current_system_state);
    case state_machine::State::ResetSlow:
      return onEnterResetSlow(current_system_state);
    case state_machine::State::TimedOut:
      return onEnterTimedout();
    case state_machine::State::Configuring:
      return onEnterConfiguring();
    default:
      Serial.println("You done did it now, boi!");
  }
}

void onRemainInState(state_machine::State current_state, peripherals::SystemState current_system_state)
{
  switch(current_state)
  {
    case state_machine::State::NotReady:
      return onRemainInNotReady(current_system_state);
    case state_machine::State::Configuring:
      return onRemainInConfiguring(current_system_state);
    case state_machine::State::Forward:
      return onRemainInForward(current_system_state);
    case state_machine::State::ResetFast:
      return onRemainInResetFast(current_system_state);
    case state_machine::State::ResetSlow:
    return onRemainInResetSlow(current_system_state);
  }
}

void onEnterInit()
{
  Serial.println("State: Init");
  peripherals::setLights(true, true);
}

void onEnterNotReady()
{
  Serial.println("State: Not Ready");
  peripherals::setLights(false, true);

  // Blink state might have changed in configuration
  peripherals::setLightBlinkInterval(seconds(1));
}

void onRemainInNotReady(peripherals::SystemState current_state)
{
  if(current_state.jog_left)
  {
    peripherals::setMotor(peripherals::MotorState::JogReverse);
  }
  else if(current_state.jog_right)
  {
    peripherals::setMotor(peripherals::MotorState::JogForward);
  }
  else
  {
    peripherals::setMotor(peripherals::MotorState::Stop);
  }
}

void onEnterReady()
{
  Serial.println("State: Ready");
  peripherals::setLights(true, false);

  // Sprinklers might be on from preflight
  peripherals::setLaunchSprinkler(false);

  // Motor might be jogging from not ready
  peripherals::setMotor(peripherals::MotorState::Stop);

  // Blink state might have changed in configuration
  peripherals::setLightBlinkInterval(seconds(1));

  // If we just finished resetting, then we were collecting stats during
  stopCollectingAndOutputStats();
}

void onEnterConfiguring()
{
  Serial.println("State: Configuring");
  peripherals::setLights(false, true, true);

  setBlinkSpeedFromConfigIndex();

  // Ignore any buffered UDP messages to prevent weird behaviors
  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  while(udp_client.parsePacket())
  {
    udp_client.flush();
  }
}

// Used to detect go button rising edge in config. Maybe refactor later?
bool last_go_button_in_config = false;
void onRemainInConfiguring(peripherals::SystemState current_state)
{
  // Motor jog controls
  if(current_state.jog_left)
  {
    peripherals::setMotor(peripherals::MotorState::JogReverse);
  }
  else if(current_state.jog_right)
  {
    peripherals::setMotor(peripherals::MotorState::JogForward);
  }
  else
  {
    peripherals::setMotor(peripherals::MotorState::Stop);
  }

  // Check if the "change config" button got hit
  if(current_state.go_button && !last_go_button_in_config)
  {
    setConfigIndex((current_config_index + 1) % config::PRESETS.size());
  }

  last_go_button_in_config = current_state.go_button;
}

void onEnterPreflight()
{
  Serial.println("State: Preflight");

  peripherals::setLights(true, true, true);
  setBlinkSpeedFromConfigIndex();

  peripherals::setLaunchSprinkler(true);
}

rtos_clock::time_point print_tick;
void onEnterForward()
{
  Serial.println("State: Forward");

  stats::start_collecting();

  peripherals::setSlideSprinkler(true);
  peripherals::resetEncoder();
  peripherals::setLights(false, true);
  peripherals::setLightBlinkInterval(seconds(1));

  ASSERT(MotorController::instance().setOutputFrequencyHz(target_output_hz));
  peripherals::setMotor(peripherals::MotorState::Forward);

  print_tick = rtos_clock::now();
}

void onRemainInForward(peripherals::SystemState current_state)
{
  if(rtos_clock::now() - print_tick > milliseconds(500))
  {
    print_tick = rtos_clock::now();
    Serial.print(current_state.encoder_position);
    Serial.print(" -> ");
    Serial.print(target_forward_counts);
    Serial.print("\n");
  }
}

void onEnterStopping()
{
  Serial.println("State: Stopping");
  peripherals::setLights(false, true, true);
  peripherals::setMotor(peripherals::MotorState::Stop);
}

void onEnterPostflight()
{
  Serial.println("State: Postflight");
  peripherals::setLights(false, true, false);
  peripherals::setSlideSprinkler(false);
}

void onEnterComplete()
{
  Serial.println("State: Complete");
  peripherals::setLights(true, false, true);
  peripherals::setMotor(peripherals::MotorState::Stop);
  peripherals::setLaunchSprinkler(false);

  stopCollectingAndOutputStats();
}

duration<float> calculateTimeout(float run_speed_hz, float target_rotations)
{
  return ride::expected_travel_time(run_speed_hz, target_rotations) * CALCULATED_TIMEOUT_RATIO;
}

void onEnterResetFast(peripherals::SystemState current_system_state)
{
  Serial.println("State: Reset Fast");

  stats::start_collecting();

  // Calculate the value that the counter should read (if it starts counting down toward 0) at the point
  // when we flip from fast reset to slow
  target_reverse_counts = current_system_state.encoder_position * (1.0 - HIGH_SPEED_REVERSE_PERCENT); 
  reset_fast_timeout = calculateTimeout(RESET_FAST_MOTOR_HZ, (current_system_state.encoder_position * HIGH_SPEED_REVERSE_PERCENT) / 10.0);

  Serial.print("Reset fast timeout ");
  Serial.println(reset_fast_timeout.count());

  peripherals::setLights(false, true);

  ASSERT(MotorController::instance().setOutputFrequencyHz(RESET_FAST_MOTOR_HZ));
  peripherals::setMotor(peripherals::MotorState::Reverse);

  print_tick = rtos_clock::now();
}

void onRemainInResetFast(peripherals::SystemState current_state)
{
  if(rtos_clock::now() - print_tick > milliseconds(500))
  {
    print_tick = rtos_clock::now();
    Serial.print(current_state.encoder_position);
    Serial.print(" -> ");
    Serial.print(target_reverse_counts);
    Serial.print("\n");
  }
}

void onEnterResetSlow(peripherals::SystemState current_system_state)
{
  Serial.println("State: Reset Slow");
  peripherals::setLights(true, true);

  reset_slow_timeout = calculateTimeout(RESET_SLOW_MOTOR_HZ, current_system_state.encoder_position / 10.0);
  Serial.print("Reset slow timeout ");
  Serial.println(reset_slow_timeout.count());

  ASSERT(MotorController::instance().setOutputFrequencyHz(RESET_SLOW_MOTOR_HZ));
  peripherals::setMotor(peripherals::MotorState::Reverse);

  print_tick = rtos_clock::now();
}

void onRemainInResetSlow(peripherals::SystemState current_state)
{
  if(rtos_clock::now() - print_tick > milliseconds(500))
  {
    print_tick = rtos_clock::now();
    Serial.print(current_state.encoder_position);
    Serial.print(" -> ");
    Serial.print(0);
    Serial.print("\n");
  }
}

void onEnterTimedout()
{
  Serial.println("State: Timed Out");

  peripherals::setLights(true, true, true);

  // Sprinklers might be on from forward
  peripherals::setLaunchSprinkler(false);
  peripherals::setSlideSprinkler(false);

  // Motor might be on from forward or reset
  peripherals::setMotor(peripherals::MotorState::Stop);

  stopCollectingAndOutputStats();
}

template<class InputIterator>
void printSamples(InputIterator begin, InputIterator end)
{
  using SampleT = typename std::iterator_traits<InputIterator>::value_type;

  std::for_each(begin, end, [](const SampleT& sample)
  {
    Serial.print(sample.timestamp.count());
    Serial.print(",");
  });
  Serial.print("\n");

  std::for_each(begin, end, [](const SampleT& sample)
  {
    Serial.print(sample.value);
    Serial.print(",");
  });
  Serial.print("\n\n");
}

template<bool v, class T>
using enable_if_t = typename std::enable_if<v, T>::type;

template<class T, enable_if_t<std::is_same<T, float>::value, int> = 1>
void writeReportSample(const stats::Sample<T>& s)
{
  const std::int32_t scaled = s.value*1000;
  const std::int16_t time = s.timestamp.count();

  udp_client.write(reinterpret_cast<const std::uint8_t*>(&time), 2);
  udp_client.write(reinterpret_cast<const std::uint8_t*>(&scaled), 4);
}

template<class T, enable_if_t<std::is_integral<T>::value, int> = 1>
void writeReportSample(const stats::Sample<T>& s)
{
  const std::int32_t value = s.value;
  const std::int16_t time = s.timestamp.count();

  udp_client.write(reinterpret_cast<const std::uint8_t*>(time), 2);
  udp_client.write(reinterpret_cast<const std::uint8_t*>(value), 4);
}

template<class InputIterator>
void publishSamples(const messaging::Parameter param, InputIterator begin, InputIterator end)
{
  if(!partner_ip_addr_set)
  {
    return;
  }

  using SampleT = typename std::iterator_traits<InputIterator>::value_type;

  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  udp_client.beginPacket(partner_ip_addr, p1am::eth::UDP_PORT_OUT);
  udp_client.write(static_cast<std::uint8_t>(messaging::UdpHeader::Stats));
  udp_client.write(static_cast<std::uint8_t>(param));

  const std::int16_t count = std::distance(begin, end);
  udp_client.write(reinterpret_cast<const std::uint8_t*>(&count), 2);

  std::for_each(begin, end, [](const SampleT& s) { writeReportSample(s); });
  udp_client.endPacket();
}

void stopCollectingAndOutputStats()
{
  const auto samples = stats::stop_collecting();

  if(samples)
  {
    Serial.print("Collected ");
    Serial.print(samples->count);
    Serial.println(" samples");

    Serial.println("Motor Status");
    printSamples(samples->motor_status.begin(), samples->motor_status.begin() + samples->count);
    publishSamples(messaging::Parameter::MotorStatus, samples->motor_status.begin(), samples->motor_status.begin() + samples->count);

    Serial.println("Motor Torque %");
    printSamples(samples->motor_torque.begin(), samples->motor_torque.begin() + samples->count);
    publishSamples(messaging::Parameter::MotorTorquePct, samples->motor_torque.begin(), samples->motor_torque.begin() + samples->count);

    Serial.println("Motor Power (Kw)");
    printSamples(samples->motor_power.begin(), samples->motor_power.begin() + samples->count);
    publishSamples(messaging::Parameter::MotorPowerKw, samples->motor_power.begin(), samples->motor_power.begin() + samples->count);

    Serial.println("Motor Output Frequency (Hz)");
    printSamples(samples->motor_frequency_out.begin(), samples->motor_frequency_out.begin() + samples->count);
    publishSamples(messaging::Parameter::MotorOutputFrequencyHz, samples->motor_frequency_out.begin(), samples->motor_frequency_out.begin() + samples->count);

    // Serial.println("Motor Actual Frequency (Hz)");
    // printSamples(samples->motor_frequency_actual.begin(), samples->motor_frequency_actual.begin() + samples->count);
    // publishSamples(messaging::Parameter::MotorActualFrequencyHz, samples->motor_frequency_actual.begin(), samples->motor_frequency_actual.begin() + samples->count);

    // Serial.println("Motor Position");
    // printSamples(samples->motor_position.begin(), samples->motor_position.begin() + samples->count);
    // publishSamples(messaging::Parameter::MotorPosition, samples->motor_position.begin(), samples->motor_position.begin() + samples->count);

    Serial.println("Encoder Position");
    printSamples(samples->encoder_position.begin(), samples->encoder_position.begin() + samples->count);
    publishSamples(messaging::Parameter::EncoderPosition, samples->encoder_position.begin(), samples->encoder_position.begin() + samples->count);

    Serial.println("Stats done!");
  }
}

// Return the value for a parameter, scaled to publish over UDP
std::int32_t getParameterValue(messaging::Parameter param);

// Set the value for a parameter if settable, from the scaled value published over UDP
bool setParameterValue(messaging::Parameter param, std::int32_t scaled_udp_value);

void sendPong()
{
  ASSERT(partner_ip_addr_set);

  constexpr static std::array<std::uint8_t, 1> pong = {{static_cast<std::uint8_t>(messaging::UdpHeader::Pong)}};

  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  udp_client.beginPacket(partner_ip_addr, p1am::eth::UDP_PORT_OUT);
  udp_client.write(pong.data(), pong.size());
  udp_client.endPacket();
}

void sendReport(messaging::Parameter param)
{
  ASSERT(partner_ip_addr_set);

  std::array<std::uint8_t, 6> report = {{static_cast<std::uint8_t>(messaging::UdpHeader::Report), static_cast<std::uint8_t>(param)}};

  const auto value = getParameterValue(param);

  std::memcpy(&report[2], &value, 4);

  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  udp_client.beginPacket(partner_ip_addr, p1am::eth::UDP_PORT_OUT);
  udp_client.write(report.data(), report.size());
  udp_client.endPacket();
}

void handleUdpPacket(bool allow_configuration)
{
  std::array<std::uint8_t, 256> udp_buffer;
  IPAddress sender_ip;
  std::size_t bytes_read = 0;

  {
    std::lock_guard<rtos_mutex> l(spi_bus_mutex);

    const auto next_udp_packet_size = udp_client.parsePacket();
    if(!next_udp_packet_size)
    {
      return;
    }

    if(next_udp_packet_size > udp_buffer.size())
    {
      Serial.print("Skipping UDP packet - too large for buffer: ");
      Serial.println(next_udp_packet_size);
      return;
    }

    bytes_read = udp_client.read(udp_buffer.data(), udp_buffer.size());
    ASSERT(bytes_read == next_udp_packet_size);

    sender_ip = udp_client.remoteIP();
    udp_client.flush();
  }

  // Track the most recent device that communicated with us as the
  // guy to send things to
  if(sender_ip != partner_ip_addr)
  {
    Serial.print("New remote UDP address: ");
    sender_ip.printTo(Serial);
    Serial.print("\n");
    partner_ip_addr = sender_ip;
    partner_ip_addr_set = true;
  }

  if(bytes_read < 1)
  {
    return;
  }

  switch(static_cast<messaging::UdpHeader>(udp_buffer[0]))
  {
    case messaging::UdpHeader::Ping:
    {
      Serial.println("Received ping!");
      sendPong();
    }
    break;
    case messaging::UdpHeader::Get:
    {
      if(bytes_read < 2)
      {
        return;
      }

      const auto param = messaging::Parameter(udp_buffer[1]);

      Serial.print("Received Get (");
      Serial.print(static_cast<std::int32_t>(param));
      Serial.println(")");

      sendReport(param);
      break;
    }
    case messaging::UdpHeader::Set:
    {
      if(bytes_read < 6)
      {
        return;
      }

      const auto param = messaging::Parameter(udp_buffer[1]);
      std::int32_t value;
      std::memcpy(&value, &udp_buffer[2], 4);

      Serial.print("Received Set (");
      Serial.print(static_cast<std::int32_t>(param));
      Serial.print(" -> ");
      Serial.print(value);
      Serial.println(")");

      if(allow_configuration)
      {
        setParameterValue(param, value);
      } else {
        Serial.println("Configuration not allowed in current state");
      }

      sendReport(param);

      break;
    }
  }
}

std::int32_t getParameterValue(messaging::Parameter param)
{
  switch(param)
  {
    case messaging::Parameter::MotorStatus:
    break;
    case messaging::Parameter::MotorRotationDirection:
    break;
    case messaging::Parameter::MotorOutputFrequencyHz:
    break;
    case messaging::Parameter::MotorActualFrequencyHz:
    break;
    case messaging::Parameter::MotorOutputCurrentA:
    break;
    case messaging::Parameter::MotorTorquePct:
    break;
    case messaging::Parameter::MotorPowerKw:
    break;
    case messaging::Parameter::MotorPosition:
    break;
    case messaging::Parameter::MotorKeypadRunKeyRouting:
    break;
    case messaging::Parameter::MotorFrequencySourceSetting:
    break;
    case messaging::Parameter::MotorModbusSourceFrequencyHz:
    break;
    case messaging::Parameter::MotorRunCommandSource:
    break;
    case messaging::Parameter::MotorBaseFrequencyHz:
    break;
    case messaging::Parameter::MotorMaxFrequencyHz:
    break;
    case messaging::Parameter::MotorJogFrequencyHz:
    break;
    case messaging::Parameter::MotorJogDcBrakeSetting:
    break;
    case messaging::Parameter::MotorDcBrakeSetting:
    break;
    case messaging::Parameter::MotorDcBrakeFrequencyHz:
    break;
    case messaging::Parameter::MotorDcBrakeWaitTimeS:
    break;
    case messaging::Parameter::MotorDcBrakeForceDecelPct:
    break;
    case messaging::Parameter::MotorDcBrakeTimeDecelS:
    break;
    case messaging::Parameter::MotorAccelProfileSwitchMode:
    break;
    case messaging::Parameter::MotorAccelTime1S:
    break;
    case messaging::Parameter::MotorDecelTime1S:
    break;
    case messaging::Parameter::MotorAccelTime2S:
    break;
    case messaging::Parameter::MotorDecelTime2S:
    break;
    case messaging::Parameter::MotorAccelCurve:
    break;
    case messaging::Parameter::MotorDecelCurve:
    break;
    case messaging::Parameter::EncoderPosition:
    break;
  }
  return 0;
}

bool setParameterValue(messaging::Parameter param, std::int32_t scaled_udp_value)
{
  switch(param)
  {
    case messaging::Parameter::MotorStatus:
    break;
    case messaging::Parameter::MotorRotationDirection:
    break;
    case messaging::Parameter::MotorOutputFrequencyHz:
    break;
    case messaging::Parameter::MotorActualFrequencyHz:
    break;
    case messaging::Parameter::MotorOutputCurrentA:
    break;
    case messaging::Parameter::MotorTorquePct:
    break;
    case messaging::Parameter::MotorPowerKw:
    break;
    case messaging::Parameter::MotorPosition:
    break;
    case messaging::Parameter::MotorKeypadRunKeyRouting:
    break;
    case messaging::Parameter::MotorFrequencySourceSetting:
    break;
    case messaging::Parameter::MotorModbusSourceFrequencyHz:
    break;
    case messaging::Parameter::MotorRunCommandSource:
    break;
    case messaging::Parameter::MotorBaseFrequencyHz:
    break;
    case messaging::Parameter::MotorMaxFrequencyHz:
    break;
    case messaging::Parameter::MotorJogFrequencyHz:
    break;
    case messaging::Parameter::MotorJogDcBrakeSetting:
    break;
    case messaging::Parameter::MotorDcBrakeSetting:
    break;
    case messaging::Parameter::MotorDcBrakeFrequencyHz:
    break;
    case messaging::Parameter::MotorDcBrakeWaitTimeS:
    break;
    case messaging::Parameter::MotorDcBrakeForceDecelPct:
    break;
    case messaging::Parameter::MotorDcBrakeTimeDecelS:
    break;
    case messaging::Parameter::MotorAccelProfileSwitchMode:
    break;
    case messaging::Parameter::MotorAccelTime1S:
    break;
    case messaging::Parameter::MotorDecelTime1S:
    break;
    case messaging::Parameter::MotorAccelTime2S:
    break;
    case messaging::Parameter::MotorDecelTime2S:
    break;
    case messaging::Parameter::MotorAccelCurve:
    break;
    case messaging::Parameter::MotorDecelCurve:
    break;
    case messaging::Parameter::EncoderPosition:
    break;
  }
  return false;
}
