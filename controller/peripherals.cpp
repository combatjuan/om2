#include "peripherals.h"

#include <P1AM.h>
#include <P1_HSC.h>
#include <FreeRTOS_SAMD21.h>

#include "motor_control.h"
#include "constants.h"
#include "thread.h"

#include <cstdint>
#include <chrono>
#include <utility>
#include <mutex>

using namespace std::chrono;

namespace peripherals
{
// We can't really discover the module ordering, but we can verify that it's
// the order we expect, which includes these modules
const static int HSC_MODULE_ID = 1;
const static int IO_MODULE_ID = 2;

const char* EXPECTED_MODULES[] = {"P1-02HSC", "P1-15CDD2"};

P1_HSC_Module HSC(HSC_MODULE_ID);

static std::uint32_t p1_discrete_output = 0;
static std::uint32_t blink_mask = 0;
static bool blink_on_next = false;
static std::chrono::microseconds light_blink_interval = std::chrono::seconds(1);

void blink_task(void*);
TaskHandle_t tid_blink;

void init()
{
  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  while(!P1.init()) {
    ;
  }

  if(P1.rollCall(EXPECTED_MODULES, 2))
  {
    Serial.println("P1AM modules are not configured correctly - init aborted");
    while(1){};
  }

  xTaskCreate(&blink_task, "blink", 256, nullptr, tskIDLE_PRIORITY+priority::BLINK_LOOP, &tid_blink);

  HSC.CNT1.isRotary = false;
  HSC.CNT1.enableZReset = false;
  HSC.CNT1.inhibitOn = false;
  HSC.CNT1.mode = stepDirection;
  HSC.CNT1.polarity = negativeDirection;

  HSC.configureChannels();

  HSC.CNT1.setPosition(0);
}

SystemState readSystemState(){
  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  const auto all_states = P1.readDiscrete(IO_MODULE_ID);

  SystemState result;

  // Channels are 1-indexed, and bits are 0-indexed; so all channels are reduced by
  // 1 to get the shift amount
  result.limit_switch = all_states & (1 << (p1am::digital::in::LIMIT_SWITCH_CHANNEL - 1));
  result.go_button = all_states & (1 << (p1am::digital::in::GO_BUTTON_CHANNEL - 1));
  result.engage_button = all_states & (1 << (p1am::digital::in::ENGAGE_BUTTON_CHANNEL - 1));
  result.configure_button = all_states & (1 << (p1am::digital::in::CONFIGURE_BUTTON_CHANNEL - 1));
  result.jog_left = all_states & (1 << (p1am::digital::in::JOG_LEFT_CHANNEL - 1));
  result.jog_right = all_states & (1 << (p1am::digital::in::JOG_RIGHT_CHANNEL - 1));

  result.encoder_position = HSC.CNT1.readPosition();

  return result;
}

void resetEncoder()
{
  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  HSC.CNT1.setPosition(0);
}

void setLightBlinkInterval(std::chrono::microseconds interval)
{
  light_blink_interval = interval;
}

void setLights(bool ready_state, bool problem_state, bool blink)
{
    constexpr std::uint32_t clear_mask = ~((1 << (p1am::digital::out::READY_LIGHT_CHANNEL - 1)) |
                                           (1 << (p1am::digital::out::PROBLEM_LIGHT_CHANNEL - 1)));

    const std::uint32_t data = (ready_state << (p1am::digital::out::READY_LIGHT_CHANNEL - 1)) |
                               (problem_state << (p1am::digital::out::PROBLEM_LIGHT_CHANNEL - 1));

    blink_mask = (blink_mask & clear_mask);

    if(blink)
    {
      blink_mask |= data;
    }

    if(!blink || !blink_on_next)
    {
      p1_discrete_output = (p1_discrete_output & clear_mask) | data;

      std::lock_guard<rtos_mutex> l(spi_bus_mutex);
      P1.writeDiscrete(p1_discrete_output, IO_MODULE_ID);
    }
}

void setMotor(MotorState new_state)
{
  constexpr std::uint32_t clear_mask = ~((1 << (p1am::digital::out::FORWARD_MOTOR_CHANNEL - 1)) |
                                        (1 << (p1am::digital::out::REVERSE_MOTOR_CHANNEL - 1)));

  std::uint32_t data = 0;
  if(new_state == MotorState::Forward)
  {
    data = (1 << (p1am::digital::out::FORWARD_MOTOR_CHANNEL - 1));
  }
  else if(new_state == MotorState::Reverse)
  {
    data = (1 << (p1am::digital::out::REVERSE_MOTOR_CHANNEL - 1));
  }

  p1_discrete_output = (p1_discrete_output & clear_mask) | data;

  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  P1.writeDiscrete(p1_discrete_output, IO_MODULE_ID);
}

void setLaunchSprinkler(bool on)
{
  constexpr std::uint32_t clear_mask = ~(1 << (p1am::digital::out::LAUNCH_SPRINKLER_CHANNEL - 1));
  const std::uint32_t data = on << (p1am::digital::out::LAUNCH_SPRINKLER_CHANNEL - 1);

  p1_discrete_output = (p1_discrete_output & clear_mask) | data;

  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  P1.writeDiscrete(p1_discrete_output, IO_MODULE_ID);
}

void setSlideSprinkler(bool on)
{
  constexpr std::uint32_t clear_mask = ~(1 << (p1am::digital::out::SLIDE_SPRINKLER_CHANNEL - 1));
  const std::uint32_t data = on << (p1am::digital::out::SLIDE_SPRINKLER_CHANNEL - 1);

  p1_discrete_output = (p1_discrete_output & clear_mask) | data;

  std::lock_guard<rtos_mutex> l(spi_bus_mutex);
  P1.writeDiscrete(p1_discrete_output, IO_MODULE_ID);
}

void blink_task(void*)
{
  while(true)
  {
    std::this_thread::sleep_for(light_blink_interval);

    if(blink_mask != 0)
    {
      if(blink_on_next)
      {
        p1_discrete_output |= blink_mask;
      }
      else
      {
        p1_discrete_output &= ~blink_mask;
      }
      blink_on_next = !blink_on_next;

      std::lock_guard<rtos_mutex> l(spi_bus_mutex);
      P1.writeDiscrete(p1_discrete_output, IO_MODULE_ID);
    }
  }
}
}