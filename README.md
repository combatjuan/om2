# Orphan Maker Mk II

## Name
Orphan Maker Mark II

## Description
Software to run a motor-accerated waterslide. 2023 edition.

## Physical System
Diagram
--------
Consult the following diagram to understand the components of the orphan-maker.
```
         D        G              F                      E      I
   o B   +o=======#====================================o+''''''''
   +-#  *#  >-+o_/   ....                               |
 __[_:_/_|__(==)____/_______                            |
   A          C      H      '''------------...__________|__
```

# A - Operator
The operator is a sober individual without malicious intent.  He or she operates the controls and is responsible for ensuring the (relative) safety of the Orphan Maker Mk II.  It is the operator who initiates fun.
* Operator

# B - Control Panel
The control panel contains the majority of the electronics for the Orphan Maker as well as the connection to power.  Mounted to the box is also a set of controls for the system.
* 240v input
* Main power switch
* Main circuit breaker
* 24v circuit
* 5v circuit
* Control circuit lock
* Motor control
* E-Stop
* Engage button
* Go button
* Secondary button
* Jog 3-way switch
* Ready Light
* Warning Light

# C - Passenger
The passenger rides an inflated tube down the acceleration corrider and is delivered to the top of the slip and slide at a safe and satisfying speed.  Their job is mostly to chortle with joy.
* Passenger
* Tube
* Smile

# D - Riblet 1
Riblet one provides the power to the system.  A 3phase AC motor turns a belt which is attached to the axle for the drive pulley which actuates the acceleration loop.  To this riblet is also mounted the braking system and several sensors.
* Riblet 1 structural beam
* Riblet 1 mount plate
* Riblet 1 foundation
* Riblet 1 guy wire
* Rotation sensor
* Rotation sensor magnet
* Return switch
* Motor
* Motor mount
* Motor drive belt pulley
* Motor belt
* Drive fork
* Drive axle
* Drive axle belt pulley

# E - Riblet 2
Riblet is at the other end of the pulley from Riblet 1.  It mostly holds the pulley in tension, aided by guy wires and provides mount points for the overhead sprinkler system.
* Riblet 2 bottom upright beam
* Riblet 2 top upright beam
* Riblet 2 pulley arm
* Riblet 2 foundation
* Riblet 2 mount plate
* Return pulley
* Return pull axle
* Return pulley guide pulleys
* Return pulley fork
* Return pulley mount
* Arm guy wire attachment
* Pole guy wire attachment
* Overhead sprinkler attachment
* Riblet 2 base
* Arm guy wire
* Upright guy wire

# F - Acceleration Loop
A steel cable makes a circuit around the drive pulley and the return pulley.  Attached to this cable is a carriage from which runs a pull line which is a ski-rope that the passenger holds until attaining appropriate acceleration.
* Cable
* Turnbuckle

# G - Acceleration Carriage
A structure that is affixed to the acceleration loop providing stability to the loop as the passenger proceeds along it.
* Return switch actuator
* Aluminum chassis
* Guide wheels
* Pull line attachment
* Pull line
* Pull handle

# H - Automated Sprinkler System
Three high power directed sprinklers controlled by a ball valve which provide wetness and entertainment when activated and keep the ground from being oversoaked when not needed.
* Electronically operated ball valve
* 3 Directable mount points
* High power water dispersement heads

# I - Overhead sprinkler system
A cable tensioned over the rest of the slide which holds a sprinkler hose and keeps the slide moist.  Always on.
* A few hundred feet of cable
* A few hundred feed of soaker hose

System Inputs
------------
## Power switch
The entire system must be powered.  This is gated by a power switch which controls power to everything within the control panel.

## E-Stop
Does not involve software.  Immediately cuts the control signal to the motor which will cause it to come to a powered stop.  The braking system will operate on battery.

Software Inputs
------
## Engage Button
When this button is held the system may be engaged.  Used primarily as a safety control.

## Go Button
Actuates the acceleration of the passenger and later the reset of the system.

## Configure Button
A yellow button unused in the 2021 configuration that was originally intended for returning the ride to ready condition but is used for configuration in 2023.

## Return switch
A limit switch which indicates that the acceleration loop has returned to the proper position.

## Rotation sensor
Indicates each time the drive pulley passes a particular point.  We keep track of forward and reverse rotation as two seperate outputs from the same physical sensor.

Other Controls
------
## Jog switch
Allows the acceleration loop to be manually shifted forward or backwards.  This will not be directly software controlled but the software will be able to enable and disable it.

Software Outputs
-------
## Motor go forward
When high the motor should accelerate at the set rate.
When both motor forward and motor reverse are low, the motor will actively stop.

## Motor go reverse
When high the motor should accelerate in the reverse direction at as set rate.
When both motor forward and motor reverse are low, the motor will actively stop.

## Green Light
Light the green button.  Positive connotations.

## Amber Light
Light the amber button.  Caution connotations.

## Sprinklers On
Activates sprinkler system

Non-Software Outputs
------
## Jog forward
Drive jog forward pin high

## Jog reverse
Drive jog reverse pin high

Configuration
-------------
Configuration is divided into three categories:
1. The physical geometry of the system
2. The constant motor configuration
3. The variable motor configuration to customize the ride

## Geometry
```
   |                             G                         |
      A B  C   D                    E                    F
   |  | |    |   |                                 |       |
 H O==^**=====###==========================================O
   |
[]-o  I
```

### A Return switch position
The number of meters in front of the center of the drive pulley that the expected start position is.

### B Return trigger length
The length of the object which triggers the limit switch

### C Return trigger distance
The distance from the limit trigger to the rear of the carriage

### D Carriage Length
The length of the carriage

### E Ride Length
The total distance we expect to be controlling the carriage over including acceleration distance, constant speed distance, and deceleration distance.

### F Chaos Buffer
An extra length we include to make sure we're leaving some buffer for slower than expected acceleration.

### G Line length
The full length of the drive line system from center of pulley to center of pulley.
Used as a check and should match return switch position + return trigger length + return trigger distance + carriage length + ride length + chaos buffer 

### H Loop pulley diameter
Diameter of the driven pulley that is connected to the acceleration loop.  Used to calculate distance travelled.

### I Motor to pulley ratio
The number of motor revolutions required to turn the drive pulley exactly once.  In practice this is the ratio between the motor pulley to the driving pulley on the drive axle.  Used to convert motor speed to rider speed.

## Motor Configuration
### See PARAMETERS.md
This includes settings such as DC braking profile and so forth.

## Ride Configuration
### Acceleration length/time
The full lenght of acceleration in meters.

### Braking length/time
How many meters are allowed for braking in meters.

### Maximum speed
Target max speed at end of acceleration in meters per second.

State Machine
-------------
``` mermaid
flowchart TD
    StateOff[Off];
    StateInit[Init];
    StateUnready[Unready];
    StateReady[Ready];
    StatePreflight[Preflight];
    StateForward[Forward];
    StateStopping[Stopping]
    StatePostFlight[Postflight]
    StateComplete[Complete];
    StateReturnFast[Return Fast];
    StateReturnSlow[Return Slow];
    StateConfigure[Configure];
    StateTimeout[Timeout]
    StateAnyError([Any Error Event]);
	StateError[Error]

    %% State Flow
    StateOff--Power On-->StateInit;
    StateInit-->StateUnready;
    StateUnready--Return Switch On-->StateReady;
    StateReady--Engage Button Held-->StatePreflight;
    StatePreflight--Engage Button Released-->StateReady;
    StatePreflight--Go Button-->StateForward;
    StateForward--Counter-->StateStopping;
    StateForward--Timer-->StateTimeout;
    StateStopping--Timer-->StatePostFlight;
    StatePostFlight--Timer-->StateComplete
    StateComplete--Go Button-->StateReturnFast;
    StateReturnFast--Counter-->StateReturnSlow;
    StateReturnFast--Timer-->StateTimeout;
    StateReturnFast--Return Switch On-->StateReady;
    StateReturnSlow--Return Switch On-->StateReady;
    StateReturnSlow--Timer-->StateTimeout;
    StateTimeout--Configure Button-->StateUnready
	%% Configuration subtree
    StateReady--Configure Button-->StateConfigure;
    StateUnready--Configure Button-->StateConfigure;
    StateConfigure--Configure Button and Returned-->StateReady;
    StateConfigure--Configure Button and Not Returned-->StateUnready;
	%% Error subtree
	StateAnyError--Failed assert-->StateError;
	StateAnyError--Failed communication-->StateError;
```

Errata
------
## Roadmap
2024 - Upgrade to rocket sleds
2025 - Upgrade to rail gun propulsion

## Authors and acknowledgment
Andrew Stelter
Charles Harwood

## License
MIT

## Project status
Initial Prototype
