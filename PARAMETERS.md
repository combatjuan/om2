# Overview
The Hitachi WJ200 Inverter is highly configurable.  Here are parameters we care about particularly.

## Parameter chart
| FC   | Rgstr    | Scale  | Default   | Unit | Name |
| :--- | :------: | :----: | :-------: | :--: | :--------------:  |
|    - | 0x0003   |        |       -   | Enum | Inverter status (Stopped, coasting, braking, etc.) |
| d001 | 0x0001   |   0.01 |       -   | Hz   | Frequency Source  |
| d002 | 0x1003   |   0.01 |       -   | Amps | Output current monitor |
| d003 | 0x1004   |  0,1,2 |       -   | SFR  | Rotation Direction |
| d008 | 0x100B   |  0.01  |       -   | Hz   | Actual Frequency |
| d012 | 0x1010   |  1     |       -   | %    | Torque monitor |
| d014 | 0x1012   |  0.1   |       -   | Kw   | Power monitor |
| d030 | 0x1038   |  1     |       -   | ?    | Position feedback monitor (32bit) |
|      |          |        |           |      | |
| F001 | 0x1001-2 |    0.1 |       0.0 | Hz   | Output frequency |
| F002 | 0x1103-4 |   0.01 |      1000 | S    | Acceleration time 1 |
| F003 | 0x1105-6 |   0.01 |      1000 | S    | Deceleration time 1 |
| F004 | 0x1107   |        |         0 | FR   | Keypad run key routing (forward) |
|      |          |        |           |      | |
| A001 | 0x1201   |   0-10 |         2 |      | Frequency source (Value of F001) |
| A002 | 0x1202   |    1-4 |         1 |      | Run command source (Control circuit terminal block) |
| A003 | 0x1203   |    0.1 |        60 | Hz   | Base frequency |
| A004 | 0x1204   |    0.1 |        60 | Hz   | Max frequency |
| A038 | 0x1238   |   0.01 |         5 | Hz   | Jog frequency |
| A039 | 0x1239   |    0-5 |        2? |      | Jog frequency (DC brake after jogging stops [disable during operation] |
| A051 | 0x1245   |    0-2 |         1 | NYF  | DC braking enable |
| A052 | 0x1246   |   0.01 |         0 | Hz   | DC braking frequency |
| A053 | 0x1247   |    0.1 |         0 | S    | DC braking wait time |
| A054 | 0x1248   |      1 |       100 | %    | DC braking force during deceleration |
| A055 | 0x1249   |    0.1 |       1.0 | S    | DC braking time for deceleration |
| A092 | 0x1274-5 |   0.01 |      1000 | S    | Acceleration time 2 |
| A093 | 0x1276-7 |   0.01 |       100 | S    | Deceleration time 2 |
| A094 | 0x1278   |      1 |       -   | Enum | Acceleration Profile Switch Mode (Forward/Reverse) |
| A097 | 0x127D   |    0-4 |         0 |      | Acceleration curve (linear) |
| A098 | 0x127E   |    0-4 |         0 |      | Deceleration curve (linear) |
| C001 | 0x1401   |   0-86 |         0 | Enum | Intelligent Pin 1 Setting (Forward) |
| C002 | 0x1402   |   0-86 |         1 | Enum | Intelligent Pin 1 Setting (Reverse) |
| C003 | 0x1403   |   0-86 |         6 | Enum | Intelligent Pin 1 Setting (Jog) |
